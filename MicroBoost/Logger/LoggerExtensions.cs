using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Filters;

namespace MicroBoost.Logger
{
    public static class LoggerExtensions
    {
        private const string LoggerSectionName = "Logger";
        private const string AppSectionName = "App";
        private static readonly LoggingLevelSwitch LoggingLevelSwitch = new();

        public static IWebHostBuilder UseLogger(this IWebHostBuilder webHostBuilder)
        {
            return webHostBuilder.UseSerilog((context, loggerConfiguration) =>
            {
                var appOptions = context.Configuration.GetOptions<AppOptions>(AppSectionName);
                var loggerOptions = context.Configuration.GetOptions<LoggerOptions>(LoggerSectionName);

                LoggingLevelSwitch.MinimumLevel = LogEventLevel.Information;

                loggerConfiguration.Enrich.FromLogContext()
                    .MinimumLevel.ControlledBy(LoggingLevelSwitch)
                    .Enrich.WithProperty("Application", appOptions.Name);

                loggerConfiguration.WriteTo.Console();
                if (loggerOptions.Seq is not null)
                {
                    loggerConfiguration.WriteTo.Seq(loggerOptions.Seq.Host,
                        apiKey: loggerOptions.Seq.ApiKey,
                        controlLevelSwitch: LoggingLevelSwitch);
                }

                foreach (var (key, value) in loggerOptions.MinimumLevelOverrides ?? new Dictionary<string, string>())
                {
                    var logLevel = GetLogEventLevel(value);
                    loggerConfiguration.MinimumLevel.Override(key, logLevel);
                }

                loggerOptions.ExcludePaths?.ToList().ForEach(p => loggerConfiguration.Filter
                    .ByExcluding(Matching.WithProperty<string>("RequestPath", n => n.EndsWith(p))));
            }).ConfigureLogging((_, logging) => { logging.ClearProviders(); });
        }

        private static LogEventLevel GetLogEventLevel(string level)
            => Enum.TryParse<LogEventLevel>(level, true, out var logLevel)
                ? logLevel
                : LogEventLevel.Information;
    }
}