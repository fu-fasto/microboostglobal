﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Serilog.Context;

namespace MicroBoost.Logger
{
    public class ScopedSerilogSpecificLoggingMiddleware
    {
        const string CorrelationIdHeaderName = "CorrelationID";
        private readonly RequestDelegate _next;
        private readonly ILogger<ScopedSerilogSpecificLoggingMiddleware> _logger;

        public ScopedSerilogSpecificLoggingMiddleware(RequestDelegate next, ILogger<ScopedSerilogSpecificLoggingMiddleware> logger)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task Invoke(HttpContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            var correlationId = GetOrAddCorrelationHeader(context);

            try
            {
                //Add as many nested usings as needed, for adding more properties 
                using(LogContext.PushProperty(CorrelationIdHeaderName, correlationId, true))
                {
                    await _next.Invoke(context);
                }
            }
            //To make sure that we don't loose the scope in case of an unexpected error
            catch (Exception ex) when (LogOnUnexpectedError(ex))
            {
                return;
            }
        }

        private string GetOrAddCorrelationHeader(HttpContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            if(string.IsNullOrWhiteSpace(context.Request.Headers[CorrelationIdHeaderName]))
                context.Request.Headers[CorrelationIdHeaderName] = Guid.NewGuid().ToString();

            return context.Request.Headers[CorrelationIdHeaderName];
        }

        private bool LogOnUnexpectedError(Exception ex)
        {
            _logger.LogError(ex, "An unexpected exception occured!");
            return true;
        }
    }
}