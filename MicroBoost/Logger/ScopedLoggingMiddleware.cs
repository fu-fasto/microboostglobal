﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace MicroBoost.Logger
{
    public class ScopedLoggingMiddleware
    {
        const string CorrelationIdHeaderName = "CorrelationID";
        private readonly RequestDelegate _next;
        private readonly ILogger<ScopedLoggingMiddleware> _logger;

        public ScopedLoggingMiddleware(RequestDelegate next, ILogger<ScopedLoggingMiddleware> logger)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task Invoke(HttpContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            var correlationId = GetOrAddCorrelationHeader(context);

            try
            {
                var loggerState = new Dictionary<string, object>
                {
                    [CorrelationIdHeaderName] = correlationId
                    //Add any number of properties to be logged under a single scope
                };

                using (_logger.BeginScope(loggerState))
                {
                    await _next(context);
                }
            }
            //To make sure that we don't loose the scope in case of an unexpected error
            catch (Exception ex) when (LogOnUnexpectedError(ex))
            {
                return;
            }
        }

        private string GetOrAddCorrelationHeader(HttpContext context)
        {
            if (context == null) throw new ArgumentNullException(nameof(context));

            if(string.IsNullOrWhiteSpace(context.Request.Headers[CorrelationIdHeaderName]))
                context.Request.Headers[CorrelationIdHeaderName] = Guid.NewGuid().ToString();

            return context.Request.Headers[CorrelationIdHeaderName];
        }

        private bool LogOnUnexpectedError(Exception ex)
        {
            _logger.LogError(ex, "An unexpected exception occured!");
            return true;
        }
    }
}