﻿using System.Collections.Generic;

namespace MicroBoost.Logger
{
    public class LoggerOptions
    {
        public string Level { get; set; }
        public SeqOptions Seq { get; set; }
        public IDictionary<string, string> MinimumLevelOverrides { get; set; }
        public IEnumerable<string> ExcludePaths { get; set; }
    }

    public class SeqOptions
    {
        public string Host { get; set; }
        public string ApiKey { get; set; }
    }
}