﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Jaeger.Core;
using Microsoft.Extensions.DependencyInjection;
using OpenTracing;
using SimpleInjector;

namespace MicroBoost.Helpers
{
    public static class MicroBoostHelpers
    {
        internal static Container Container { get; set; }
        private static readonly Random Random = new Random();

        public static void CompleteQuery(this IFindQuery<object> findQuery, List<RawSort> rawSorts,
            IDictionary<string, Expression<Func<object, object>>> dict)
        {
            rawSorts?.ForEach(rawSort =>
            {
                if (dict.TryGetValue(rawSort.Field, out var selector))
                    findQuery.SortExpressions.Add(new SortExpression<object>
                    {
                        Selector = selector,
                        IsAscending = rawSort.IsAscending
                    });
            });
        }

        public static List<PropertyInfo> GetPropertiesInfosHasAttribute(this Type classType, Type attributeType)
        {
            return classType
                .GetProperties()
                .Where(x => x.GetCustomAttribute(attributeType) is not null)
                .ToList();
        }

        public static List<Type> GetTypesHaveGenericInterface(this Assembly assembly, Type genericInterfaceType)
        {
            return assembly.GetTypes().ToList().Where(type =>
            {
                try
                {
                    return type.GetInterfaces()[0].GetGenericTypeDefinition() == genericInterfaceType;
                }
                catch (Exception)
                {
                    return false;
                }
            }).ToList();
        }

        public static object CreateInstance(this Type type)
        {
            return Activator.CreateInstance(type);
        }

        public static ISpan GetCurrentSpan()
        {
            return GetTracingContext()?.CurrentSpan;
        }

        public static TracingContext GetTracingContext()
        {
            try
            {
                return Container.GetRequiredService<TracingContext>();
            }
            catch
            {
                return null;
            }
        }

        public static List<List<T>> ChunkBy<T>(this List<T> source, int chunkSize)
        {
            return source
                .Select((x, i) => new {Index = i, Value = x})
                .GroupBy(x => x.Index / chunkSize)
                .Select(x => x.Select(v => v.Value).ToList())
                .ToList();
        }

        public static string RandomCode(int length = 6)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
                .Select(s => s[Random.Next(s.Length)]).ToArray());
        }
    }
}