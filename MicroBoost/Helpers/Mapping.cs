﻿using Mapster;

namespace MicroBoost.Helpers
{
    public static class Mapping
    {
        public static TTarget MapTo<TTarget>(this object source)
            where TTarget : class
        {
            return source.Adapt<TTarget>();
        }
    }
}