﻿using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using Newtonsoft.Json;

namespace MicroBoost.Caching
{
    class QueryBusCachingDecorator : IQueryBus
    {
        private readonly IQueryBus _queryBus;
        private readonly ICachingService _cachingService;

        private static readonly JsonSerializerSettings JsonSerializerSettings = new()
        {
            TypeNameHandling = TypeNameHandling.Objects,
            NullValueHandling = NullValueHandling.Ignore
        };

        public QueryBusCachingDecorator(IQueryBus queryBus, ICachingService cachingService)
        {
            _queryBus = queryBus;
            _cachingService = cachingService;
        }

        public async Task<TResponse> SendAsync<TResponse>(IQuery<TResponse> query,
            CancellationToken cancellationToken = default)
        {
            var rawQuery = JsonConvert.SerializeObject(query, JsonSerializerSettings);
            var cachedResult = await _cachingService.GetAsync<TResponse>(rawQuery, cancellationToken);
            if (cachedResult is not null) return cachedResult;

            var result = await _queryBus.SendAsync(query, cancellationToken);
            await _cachingService.SetAsync(rawQuery, result, cancellationToken);
            return result;
        }
    }
}