﻿namespace MicroBoost.Caching
{
    public class CachingOptions
    {
        public string Type { get; set; }
        public string ConnectionString { get; set; }
        public bool QueryCaching { get; set; }
    }
}