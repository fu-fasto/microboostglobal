﻿using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace MicroBoost.Caching
{
    public class CachingService : ICachingService
    {
        private readonly IDistributedCache _cache;

        public CachingService(IDistributedCache cache)
        {
            _cache = cache;
        }

        public async Task<string> GetAsync(string key, CancellationToken cancellationToken = default)
        {
            return await _cache.GetStringAsync(key, cancellationToken);
        }

        public async Task<T> GetAsync<T>(string key, CancellationToken cancellationToken)
        {
            var value = await _cache.GetStringAsync(key, cancellationToken);

            return value is null ? default : JsonConvert.DeserializeObject<T>(value);
        }

        public Task SetAsync<T>(string key, T value, CancellationToken cancellationToken)
        {
            var stringValue = JsonConvert.SerializeObject(value);
            return _cache.SetAsync(key, Encoding.UTF8.GetBytes(stringValue), new()
            {
                AbsoluteExpirationRelativeToNow = TimeSpan.FromHours(1)
            }, cancellationToken);
        }

        public Task RemoveAsync<T>(string key, CancellationToken cancellationToken)
        {
            return _cache.RemoveAsync(key, cancellationToken);
        }
    }
}