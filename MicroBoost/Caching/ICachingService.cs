﻿using System.Threading;
using System.Threading.Tasks;

namespace MicroBoost.Caching
{
    public interface ICachingService
    {
        Task<string> GetAsync(string key, CancellationToken cancellationToken = default);
        
        Task<T> GetAsync<T>(string key, CancellationToken cancellationToken = default);

        Task SetAsync<T>(string key, T value, CancellationToken cancellationToken = default);
        
        Task RemoveAsync<T>(string key, CancellationToken cancellationToken = default);
    }
}