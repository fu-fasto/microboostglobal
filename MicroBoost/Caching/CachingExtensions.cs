﻿using System;
using MicroBoost.Cqrs.Queries;
using Microsoft.Extensions.DependencyInjection;

namespace MicroBoost.Caching
{
    public static class CachingExtensions
    {
        private const string SectionName = "Caching";

        public static IMicroBoostBuilder AddCaching(this IMicroBoostBuilder builder)
        {
            var options = builder.GetOptions<CachingOptions>(SectionName);

            switch (options.Type.ToLower())
            {
                case "redis":
                    builder.Services.AddStackExchangeRedisCache(o => { o.Configuration = options.ConnectionString; });
                    break;

                case "memory":
                    builder.Services.AddDistributedMemoryCache();
                    break;

                default:
                    throw new Exception($"Caching type '{options.Type}' is not supported");
            }

            if (options.QueryCaching)
            {
                builder.Container.RegisterDecorator<IQueryBus, QueryBusCachingDecorator>();
            }

            builder.Container.RegisterSingleton<ICachingService, CachingService>();

            return builder;
        }
    }
}