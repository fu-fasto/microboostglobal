﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using SimpleInjector;

namespace MicroBoost.Cqrs
{
    public class CqrsRegistry
    {
        private static readonly IDictionary<Type, Type> HandlerTypes = new Dictionary<Type, Type>();
        private readonly IMicroBoostBuilder _microBoostBuilder;
        private readonly Container _container;
        
        public CqrsRegistry(IMicroBoostBuilder microBoostBuilder, Container container)
        {
            _microBoostBuilder = microBoostBuilder;
            _container = container;
        }

        public Type GetHandlerType(Type type)
        {
            return HandlerTypes[type];
        }

        public async Task<TResponse> HandleQuery<TResponse>(IQuery<TResponse> query, CancellationToken cancellationToken)
        {
            var genericQueryHandlerType = HandlerTypes[query.GetType()];
            var method = (Task<TResponse>) genericQueryHandlerType.GetMethod("Handle")
                ?.Invoke(_container.GetInstance(genericQueryHandlerType), new object[] {query, cancellationToken});
            if (method != null) return await method;
            return default;
        }
        
        public async Task PreHandleCommand(ICommand command, CancellationToken cancellationToken)
        {
            var genericCommandHandlerType = HandlerTypes[command.GetType()];
            var method = (Task) genericCommandHandlerType.GetMethod("PreHandle")
                ?.Invoke(_container.GetInstance(genericCommandHandlerType), new object[] {command, cancellationToken});
            if (method != null) await method;
        }
        
        public async Task HandleCommand(ICommand command, CancellationToken cancellationToken)
        {
            var genericCommandHandlerType = HandlerTypes[command.GetType()];
            var method = (Task) genericCommandHandlerType.GetMethod("Handle")
                ?.Invoke(_container.GetInstance(genericCommandHandlerType), new object[] {command, cancellationToken});
            if (method != null) await method;
        }
        
        public static void InitHandlers(Assembly callingAssembly, Container container)
        {
            callingAssembly
                .GetTypesHaveGenericInterface(typeof(IQueryHandler<,>))
                .ForEach(queryHandler =>
                {
                    var queryRequestType = queryHandler.GetInterfaces()[0].GetGenericArguments()[0];
                    var queryResponseType = queryHandler.GetInterfaces()[0].GetGenericArguments()[1];
                    var genericQueryHandlerType =
                        typeof(IQueryHandler<,>).MakeGenericType(queryRequestType, queryResponseType);
                    
                    HandlerTypes.Add(queryRequestType, genericQueryHandlerType);
                    container.Register(genericQueryHandlerType, queryHandler);
                });
            
            callingAssembly
                .GetTypesHaveGenericInterface(typeof(ICommandHandler<>))
                .ForEach(commandHandler =>
                {
                    var commandType = commandHandler.GetInterfaces()[0].GetGenericArguments()[0];
                    var genericCommandHandlerType = typeof(ICommandHandler<>).MakeGenericType(commandType);
                    
                    HandlerTypes.Add(commandType, genericCommandHandlerType);
                    container.Register(genericCommandHandlerType, commandHandler);
                });
        }
    }
}