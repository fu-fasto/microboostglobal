﻿namespace MicroBoost.Cqrs
{
    public class CqrsOptions
    {
        public bool UseMessageQueueCommand { get; set; }
        public string CommandQueueName { get; set; }
    }
}