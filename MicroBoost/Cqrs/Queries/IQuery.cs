﻿using MediatR;

namespace MicroBoost.Cqrs.Queries
{
    public interface IQuery<out TResponse> : IRequest<TResponse>
    {
    }
}
