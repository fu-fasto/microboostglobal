﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace MicroBoost.Cqrs.Queries
{
    public class QueryBus : IQueryBus
    {
        private readonly IMediator _mediator;
        
        public QueryBus(IMediator mediator)
        {
            _mediator = mediator;
        }

        public Task<TResponse> SendAsync<TResponse>(IQuery<TResponse> query, CancellationToken cancellationToken)
        {
            return _mediator.Send(query, cancellationToken);
        }
    }
}