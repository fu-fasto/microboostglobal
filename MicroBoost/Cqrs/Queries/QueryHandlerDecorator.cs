﻿using System.Threading;
using System.Threading.Tasks;

namespace MicroBoost.Cqrs.Queries
{
    public class QueryHandlerDecorator<TRequest, TResponse> : IQueryHandler<TRequest, TResponse> where TRequest : IQuery<TResponse>
    {
        private readonly IQueryHandler<TRequest, TResponse> _queryHandler;

        public QueryHandlerDecorator(IQueryHandler<TRequest, TResponse> queryHandler)
        {
            _queryHandler = queryHandler;
        }

        public Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken)
        {
            return _queryHandler.Handle(request, cancellationToken);
        }
    }
}