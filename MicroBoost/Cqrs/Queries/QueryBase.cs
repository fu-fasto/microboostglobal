﻿using System.Collections.Generic;
using System.ComponentModel;
using MicroBoost.Types;

namespace MicroBoost.Cqrs.Queries
{
    public abstract class PagedQueryBase<TEntity> : IQuery<PagedResult<TEntity>>
    {
        [DefaultValue(0)] public int Page { get; set; } = 0;
        [DefaultValue(20)] public int Size { get; set; } = 20;
        public string Text { get; set; }
        public List<RawSort> RawSorts { get; set; } = new ();
    }

    public abstract class AllQueryBase<TEntity> : IQuery<IEnumerable<TEntity>>
    {
        public string Text { get; set; }
        public List<RawSort> RawSorts { get; set; }
    }

    public abstract class OneQueryBase<TEntity, TId> : IQuery<TEntity> where TEntity : class, IEntity<TId>
    {
        public TId Id { get; set; }
    }

    public class RawSort
    {
        public string Field { get; set; }
        public bool IsAscending { get; set; }
    }
}