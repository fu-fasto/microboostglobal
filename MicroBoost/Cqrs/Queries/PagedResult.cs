﻿using System.Collections.Generic;

namespace MicroBoost.Cqrs.Queries
{
    public class PagedResult<TItem>
    {
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public long TotalItems { get; set; }
        public IEnumerable<TItem> Items { get; set; }
    }
}