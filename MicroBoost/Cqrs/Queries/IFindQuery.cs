﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MicroBoost.Cqrs.Queries
{
    public interface IFindQuery<TEntity>
    {
        int Page { get; }
        int Size { get; }
        string Text { get; }
        IList<Expression<Func<TEntity, bool>>> Filters { get; }
        List<SortExpression<TEntity>> SortExpressions { get; }
        SortExpression<TEntity> SortExpression { get; }
    }
}