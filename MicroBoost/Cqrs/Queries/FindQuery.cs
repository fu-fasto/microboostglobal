﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace MicroBoost.Cqrs.Queries
{
    public class FindQuery<TEntity> : IFindQuery<TEntity>
    {
        public int Page { get; set; }
        public int Size { get; set; } = 100;
        public string Text { get; set; }

        public IList<Expression<Func<TEntity, bool>>> Filters { get; } =
            new List<Expression<Func<TEntity, bool>>>();

        public List<SortExpression<TEntity>> SortExpressions { get; set; } = new();
        public SortExpression<TEntity> SortExpression { get; set; }
    }

    public class SortExpression<TEntity>
    {
        public Expression<Func<TEntity, object>> Selector { get; set; }
        public bool IsAscending { get; set; } = true;
    }
}