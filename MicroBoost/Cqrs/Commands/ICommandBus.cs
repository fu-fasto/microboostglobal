﻿using System.Threading;
using System.Threading.Tasks;

namespace MicroBoost.Cqrs.Commands
{
    public interface ICommandBus
    {
        Task SendAsync(ICommand command, CancellationToken cancellationToken = default);
    }
}
