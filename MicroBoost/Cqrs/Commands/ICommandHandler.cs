﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace MicroBoost.Cqrs.Commands
{
    public interface ICommandHandler<in TCommand> : INotificationHandler<TCommand> where TCommand : ICommand
    {
        Task PreHandle(TCommand command, CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}