﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;

namespace MicroBoost.Cqrs.Commands
{
    public class CommandBusDecorator : ICommandBus
    {
        private readonly ICommandBus _commandBus;
        private readonly IValidatorFactory _validationFactory;
        private readonly CqrsRegistry _cqrsRegistry;

        public CommandBusDecorator(ICommandBus commandBus, IValidatorFactory validationFactory,
            CqrsRegistry cqrsRegistry)
        {
            _commandBus = commandBus;
            _validationFactory = validationFactory;
            _cqrsRegistry = cqrsRegistry;
        }

        private void Validate(ICommand command)
        {
            var validator = _validationFactory.GetValidator(command.GetType());
            var result = validator?.Validate(new ValidationContext<ICommand>(command));

            if (result != null && !result.IsValid)
            {
                throw new ValidationException(result.Errors);
            }
        }

        public async Task SendAsync(ICommand command, CancellationToken cancellationToken = default)
        {
            Validate(command);
            await _cqrsRegistry.PreHandleCommand(command, cancellationToken);
            await _commandBus.SendAsync(command, cancellationToken);
        }
    }
}