﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.MessageBroker;

namespace MicroBoost.Cqrs.Commands
{
    public class MessageQueueCommandBus : ICommandBus
    {
        private readonly IMessagePublisher _messagePublisher;

        public MessageQueueCommandBus(IMessagePublisher messagePublisher)
        {
            _messagePublisher = messagePublisher ??
                                throw new Exception($"Missing dependency '{nameof(IMessagePublisher)}'");
        }

        public async Task SendAsync(ICommand command, CancellationToken cancellationToken = default)
        {
            await _messagePublisher.PublishAsync(command, cancellationToken);
        }
    }
}