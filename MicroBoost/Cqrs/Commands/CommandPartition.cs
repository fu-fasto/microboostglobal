﻿namespace MicroBoost.Cqrs.Commands
{
    public class CommandPartition
    {
        public int Partition { get; set; }
        public long Offset { get; set; }
    }
}