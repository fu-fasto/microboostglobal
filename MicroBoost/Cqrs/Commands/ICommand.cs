﻿using MediatR;
using MicroBoost.MessageBroker;

namespace MicroBoost.Cqrs.Commands
{
    public interface ICommand : IMessage, INotification
    {
    }
}
