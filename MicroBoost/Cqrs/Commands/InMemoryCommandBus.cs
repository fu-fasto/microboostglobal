﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MediatR;

namespace MicroBoost.Cqrs.Commands
{
    public class InMemoryCommandBus : ICommandBus
    {
        private readonly IMediator _mediator;

        public InMemoryCommandBus(IMediator mediator)
        {
            _mediator = mediator ?? throw new Exception($"Missing dependency '{nameof(IMediator)}'");
        }

        public Task SendAsync(ICommand command, CancellationToken cancellationToken = default)
        {
            return _mediator.Publish(command, cancellationToken);
        }
    }
}