﻿using MicroBoost.MessageBroker;

namespace MicroBoost.Cqrs.Commands
{
    [Message(MessageScope.Both)]
    public abstract class CommandBase : ICommand
    {
    }
}