﻿using System;
using System.Reflection;
using MediatR;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Jaeger;
using SimpleInjector;

namespace MicroBoost.Cqrs
{
    public static class CqrsExtensions
    {
        private const string Section = "Cqrs";

        public static IMicroBoostBuilder AddCqrs(this IMicroBoostBuilder builder)
        {
            var cqrsOptions = builder.GetOptions<CqrsOptions>(Section);
           
            // MediatR
            builder.Container.RegisterSingleton<IMediator, Mediator>();
            builder.Container.Register(typeof(IRequestHandler<,>), Assembly.GetCallingAssembly());
            builder.Container.Register(() => new ServiceFactory(builder.Container.GetInstance), Lifestyle.Singleton);
            builder.Container.Collection.Register(typeof(IPipelineBehavior<,>), new Type[] { });
            RegisterHandlers(builder.Container, typeof(INotificationHandler<>), Assembly.GetCallingAssembly());

            // Base Cqrs
            builder.Container.RegisterSingleton<CqrsRegistry>();
            builder.Container.RegisterSingleton<IQueryBus, QueryBus>();
            builder.Container.RegisterDecorator<ICommandBus, CommandBusDecorator>();
            builder.Container.Register<CommandPartition>();
            CqrsRegistry.InitHandlers(Assembly.GetCallingAssembly(), builder.Container);
            
            // Check message queue is used?
            if (cqrsOptions.UseMessageQueueCommand)
            {
                builder.Container.RegisterSingleton<ICommandBus, MessageQueueCommandBus>();
            }
            else
            {
                builder.Container.RegisterSingleton<ICommandBus, InMemoryCommandBus>();
            }
            
            // Add tracing decorator
            JaegerExtensions.AddCqrsDecorator(cqrsOptions);

            return builder;
        }

        private static void RegisterHandlers(Container container, Type collectionType, params Assembly[] assemblies)
        {
            var handlerTypes = container.GetTypesToRegister(collectionType, assemblies, new TypesToRegisterOptions
            {
                IncludeGenericTypeDefinitions = true,
                IncludeComposites = false
            });

            container.Collection.Register(collectionType, handlerTypes);
        }
    }
}