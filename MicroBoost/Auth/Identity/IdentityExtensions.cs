﻿using System;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace MicroBoost.Auth.Identity
{
    public static class IdentityExtensions
    {
        internal static IMicroBoostBuilder AddIdentity
            (this IMicroBoostBuilder builder, IdentityOptions identityOptions)
        {
            builder.Services
                .AddIdentity<ApplicationUser, IdentityRole>(options =>
                {
                    options.SignIn.RequireConfirmedAccount = false;
                    options.SignIn.RequireConfirmedEmail = false;
                    options.SignIn.RequireConfirmedPhoneNumber = false;
                })
                .AddEntityFrameworkStores<IdentityContext>()
                .AddDefaultTokenProviders();

            switch (identityOptions.DatabaseType.ToLower())
            {
                case "mysql":
                    builder.Services.AddDbContext<IdentityContext>(options =>
                    {
                        options.UseMySql(identityOptions.ConnectionString,
                            new MySqlServerVersion(new Version(identityOptions.DatabaseVersion)));
                    });
                    break;

                case "memory":
                    builder.Services.AddDbContext<IdentityContext>(options =>
                    {
                        options.UseInMemoryDatabase("Identity");
                    });
                    break;
            }

            builder.Container.Register<IIdentityService, IdentityService>();

            return builder;
        }
    }
}