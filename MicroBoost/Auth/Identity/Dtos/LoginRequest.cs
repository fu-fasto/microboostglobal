﻿using System.ComponentModel.DataAnnotations;

namespace MicroBoost.Auth.Identity.Dtos
{
    public class LoginRequest
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
