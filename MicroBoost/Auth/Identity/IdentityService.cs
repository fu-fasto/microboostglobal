﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using MicroBoost.Auth.Identity.Dtos;
using MicroBoost.Auth.Jwt;
using Microsoft.AspNetCore.Identity;
using Microsoft.IdentityModel.Tokens;

namespace MicroBoost.Auth.Identity
{
    public class IdentityService : IIdentityService
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly JwtOptions _jwtOptions;

        public IdentityService(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager,
            AuthOptions authOptions,
            RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _jwtOptions = authOptions.JwtOptions;
            _roleManager = roleManager;
        }

        public async Task SeedData()
        {
            var roles = new List<IdentityRole>
            {
                new(IdentityRoles.SuperAdmin),
                new(IdentityRoles.StoreAdmin),
                new(IdentityRoles.StoreStaff),
                new(IdentityRoles.Customer)
            };
            foreach (var role in roles)
            {
                await _roleManager.CreateAsync(role);
            }

            var admin = new ApplicationUser
            {
                Email = "admin@example.com",
                FirstName = "Admin",
                LastName = "Super",
                UserName = "Admin",
                EmailConfirmed = true
            };
            await _userManager.CreateAsync(admin, "Abc123!@#");
            await _userManager.AddToRolesAsync(admin, roles.Select(role => role.Name));
        }

        public async Task<LoginResponse> Login(LoginRequest request)
        {
            var user = await _userManager.FindByEmailAsync(request.Email)
                       ?? throw new Exception($"No Accounts Registered with {request.Email}.");
            var result =
                await _signInManager.PasswordSignInAsync(user.UserName, request.Password, false,
                    lockoutOnFailure: false);
            if (!result.Succeeded)
            {
                throw new Exception($"Invalid Credentials for '{request.Email}'.");
            }

            if (!user.EmailConfirmed)
            {
                throw new Exception($"Account Not Confirmed for '{request.Email}'.");
            }

            JwtSecurityToken jwtSecurityToken = await GenerateJwToken(user);

            return new LoginResponse
            {
                Id = user.Id,
                AccessToken = new JwtSecurityTokenHandler().WriteToken(jwtSecurityToken),
                Email = user.Email,
                Role = (await _userManager.GetRolesAsync(user))[0],
                IsVerified = user.EmailConfirmed,
                RefreshToken = GenerateRefreshToken(null).Token
            };
        }
        public async Task Register(RegisterRequest request)
        {
            var user = new ApplicationUser
            {
                Email = request.Email
            };
            var userWithSameEmail = await _userManager.FindByEmailAsync(request.Email);
            if (userWithSameEmail == null)
            {
                var result = await _userManager.CreateAsync(user, request.Password);
                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, IdentityRoles.StoreStaff);
                    await _userManager.AddToRoleAsync(user, IdentityRoles.Customer);
                }
                else
                {
                    throw new Exception($"{result.Errors}");
                }
            }
            else
            {
                throw new Exception($"Email {request.Email} is already registered.");
            }
        }

        public async Task UpdateRoles(ApplicationUser user, List<string> roles)
        {
            await _userManager.AddToRolesAsync(user, roles);
        }
        private async Task<JwtSecurityToken> GenerateJwToken(ApplicationUser user)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);
            var roles = await _userManager.GetRolesAsync(user);
            var roleClaims = roles.Select(role => new Claim(ClaimTypes.Role, role)).ToList();
            var claims = new[]
                {
                    // new Claim(JwtRegisteredClaimNames.Sub, user.UserName),
                    // new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(ClaimTypes.Email, user.Email),
                    new Claim(ClaimTypes.NameIdentifier, user.Id),
                    new Claim(ClaimTypes.Role, "ROLE"),
                    new Claim("uid", user.Id),
                }
                .Union(userClaims)
                .Union(roleClaims);

            var symmetricSecurityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.Key));
            var signingCredentials = new SigningCredentials(symmetricSecurityKey, SecurityAlgorithms.HmacSha256);

            return new (
                _jwtOptions.Issuer,
                _jwtOptions.Audience,
                claims,
                expires: DateTime.UtcNow.AddMinutes(_jwtOptions.DurationInMinutes),
                signingCredentials: signingCredentials);
        }
        private string RandomTokenString()
        {
            using var rngCryptoServiceProvider = new RNGCryptoServiceProvider();
            var randomBytes = new byte[40];
            rngCryptoServiceProvider.GetBytes(randomBytes);

            return BitConverter.ToString(randomBytes).Replace("-", "");
        }
        private RefreshToken GenerateRefreshToken(string ipAddress)
        {
            return new RefreshToken
            {
                Token = RandomTokenString(),
                Expires = DateTime.UtcNow.AddDays(7),
                Created = DateTime.UtcNow,
                CreatedByIp = ipAddress
            };
        }
    }
}