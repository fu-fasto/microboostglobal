﻿namespace MicroBoost.Auth.Identity
{
    public static class IdentityRoles
    {
        public const string SuperAdmin = "SuperAdmin";
        public const string StoreAdmin = "StoreAdmin";
        public const string StoreStaff = "StoreStaff";
        public const string Customer = "Customer";
    }
}