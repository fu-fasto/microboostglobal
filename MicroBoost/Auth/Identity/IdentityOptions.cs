﻿namespace MicroBoost.Auth.Identity
{
    public class IdentityOptions : Microsoft.AspNetCore.Identity.IdentityOptions
    {
        public string DatabaseType { get; set; }
        public string DatabaseVersion { get; set; }
        public string ConnectionString { get; set; }
    }
}