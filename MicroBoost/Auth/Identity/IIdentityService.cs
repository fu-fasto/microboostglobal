﻿using System.Threading.Tasks;
using MicroBoost.Auth.Identity.Dtos;

namespace MicroBoost.Auth.Identity
{
    public interface IIdentityService
    {
        Task<LoginResponse> Login(LoginRequest request);

        Task Register(RegisterRequest request);

        Task SeedData();
    }
}