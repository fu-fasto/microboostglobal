﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using MicroBoost.Auth.Identity.Dtos;
using Microsoft.AspNetCore.Identity;

namespace MicroBoost.Auth.Identity
{
    public class ApplicationUser : IdentityUser
    {
        [MaxLength(36)]
        public override string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public List<RefreshToken> RefreshTokens { get; set; }
        public bool OwnsToken(string token)
        {
            return RefreshTokens?.Find(x => x.Token == token) != null;
        }
    }
}
