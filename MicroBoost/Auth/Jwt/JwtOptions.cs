﻿namespace MicroBoost.Auth.Jwt
{
    public class JwtOptions
    {
        public string AuthenticationScheme { get; set; }
        public string Key { get; set; }
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public int DurationInMinutes { get; set; }
    }
}
