﻿using MicroBoost.Auth.Identity;
using MicroBoost.Auth.Jwt;

namespace MicroBoost.Auth
{
    public static class AuthExtensions
    {
        private const string SectionName = "Auth";
        
        public static IMicroBoostBuilder AddAuth(this IMicroBoostBuilder builder)
        {
            var authOptions = builder.GetOptions<AuthOptions>(SectionName);
            if (authOptions.IdentityOptions is not null)
            {
                builder.AddIdentity(authOptions.IdentityOptions);
            }
            if (authOptions.JwtOptions is not null)
            {
                builder.AddJwt(authOptions.JwtOptions);
            }

            return builder;
        }
    }
}