﻿using MicroBoost.Auth.Identity;
using MicroBoost.Auth.Jwt;

namespace MicroBoost.Auth
{
    public class AuthOptions
    {
        public IdentityOptions IdentityOptions { get; set; }
        public JwtOptions JwtOptions { get; set; }
    }
}