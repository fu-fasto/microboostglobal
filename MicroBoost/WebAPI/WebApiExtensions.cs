﻿using System.Reflection;
using FluentValidation;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using SimpleInjector;

namespace MicroBoost.WebAPI
{
    public static class WebApiExtensions
    {
        public static IMicroBoostBuilder AddWebApi(this IMicroBoostBuilder builder)
        {
            builder.Services.AddHttpContextAccessor();
            builder.Services.AddControllers();
            builder.Services.AddHealthChecks();

            builder.Container.Register<IValidatorFactory, ApplicationValidatorFactory>(Lifestyle.Singleton);
            builder.Container.Register(typeof(IValidator<>), Assembly.GetCallingAssembly());
            builder.Services
                .AddMvc(opt =>
                {
                    opt.Filters.Add<ExceptionFilter>();
                })
                .ConfigureApiBehaviorOptions(opt => { opt.SuppressModelStateInvalidFilter = true; });

            return builder;
        }

        public static IApplicationBuilder UseWebApi(this IApplicationBuilder app)
        {
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/health");
            });

            return app;
        }
    }
}