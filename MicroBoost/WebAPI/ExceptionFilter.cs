﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Net;
using FluentValidation;
using MicroBoost.Helpers;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OpenTracing.Tag;

namespace MicroBoost.WebAPI
{
    public class ExceptionFilter : Attribute, IExceptionFilter
    {
        private readonly IWebHostEnvironment _env;
        private readonly ILogger<ExceptionFilter> _logger;

        public ExceptionFilter(IWebHostEnvironment env, ILogger<ExceptionFilter> logger)
        {
            _env = env;
            _logger = logger;
        }

        public virtual void OnException(ExceptionContext context)
        {
            var content = new Dictionary<string, object>
            {
                {"ErrorMessage", context.Exception.Message},
            };

            if (_env.IsDevelopment())
            {
                content.Add("Details", context.Exception.StackTrace);
            }

            var statusCode = (int) MapStatusCode(context.Exception);

            LogError(context, statusCode);

            context.Result = new ObjectResult(content);
            context.HttpContext.Response.StatusCode = statusCode;
            context.Exception = null!;
        }

        protected virtual void LogError(ExceptionContext context, int statusCode)
        {
            var errorMessage = $"{context.HttpContext.Request.Path} :: [{statusCode}] {context.Exception.Message}";

            switch (statusCode)
            {
                case >= 500:
                    _logger.LogCritical("{ErrorMessage}", errorMessage);
                    break;
                case >= 400:
                    _logger.LogError("{ErrorMessage}", errorMessage);
                    break;
                default:
                    _logger.LogWarning("{ErrorMessage}", errorMessage);
                    break;
            }

            LogJaeger(errorMessage);
        }

        protected virtual HttpStatusCode MapStatusCode(Exception ex)
        {
            return ex switch
            {
                // Status Codes
                NullReferenceException => HttpStatusCode.NotFound,
                ValidationException or DataException => HttpStatusCode.BadRequest,
                UnauthorizedAccessException => HttpStatusCode.Unauthorized,
                _ => HttpStatusCode.InternalServerError
            };
        }

        private void LogJaeger(string errorMessage)
        {
            var tracingContext = MicroBoostHelpers.GetTracingContext();
            tracingContext?.CurrentSpan?.Log(errorMessage);
            tracingContext?.CurrentSpan?.SetTag(Tags.Error, true);
            tracingContext?.FinishAllPendingSpans();
        }
    }
}