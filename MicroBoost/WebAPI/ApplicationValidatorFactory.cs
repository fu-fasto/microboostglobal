﻿using System;
using FluentValidation;
using SimpleInjector;

namespace MicroBoost.WebAPI
{
    public class ApplicationValidatorFactory : IValidatorFactory
    {
        private readonly Container _container;

        /// <summary>The constructor of the factory.</summary>
        /// <param name="container">The Simple Injector Container</param>
        public ApplicationValidatorFactory(Container container)
        {
            _container = container;
        }

        /// <summary>Gets the validator for the specified type.</summary>
        public IValidator<T> GetValidator<T>()
        {
            try
            {
                return _container.GetInstance<IValidator<T>>();
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>Gets the validator for the specified type.</summary>
        public IValidator GetValidator(Type type)
        {
            try
            {
                var validator = typeof(IValidator<>).MakeGenericType(type);
                return (IValidator) _container.GetInstance(validator);
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}