﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using SimpleInjector;

namespace MicroBoost.Jaeger.Core
{
    public class TracingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly Container _container;

        public TracingMiddleware(RequestDelegate next, Container container)
        {
            _next = next;
            _container = container;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            var tracingContext = _container.GetRequiredService<TracingContext>();
            var rawSpanContext = httpContext.Request.Headers["x-root-span-context"].ToString();
            tracingContext.TryBuildRootSpanContext(rawSpanContext);

            await _next(httpContext);
        }
    }
}