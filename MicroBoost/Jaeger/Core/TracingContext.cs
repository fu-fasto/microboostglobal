﻿using System;
using System.Collections.Generic;
using Jaeger;
using OpenTracing;

namespace MicroBoost.Jaeger.Core
{
    public class TracingContext
    {
        public ISpanContext RootSpanContext { get; private set; }
        public ISpan CurrentSpan { get; private set; }
        private readonly Stack<ISpan> _spans = new();

        public void TryBuildRootSpanContext(string raw)
        {
            try
            {
                RootSpanContext = SpanContext.ContextFromString(raw);
            }
            catch (Exception)
            {
                // ignored
            }
        }
        
        public void AddSpan(ISpan span)
        {
            _spans.Push(span);
            CurrentSpan = span;
        }

        public void RevertSpan()
        {
            if (_spans.Count > 0)
            {
                _spans.Pop();
            }
            CurrentSpan = _spans.Count > 0 ? _spans.Peek() : null;
        }

        public void FinishAllPendingSpans()
        {
            while (_spans.Count > 0)
            {
                _spans.Pop().Finish();
            }
        }
    }
}