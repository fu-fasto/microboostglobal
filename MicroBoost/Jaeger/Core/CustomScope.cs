﻿using OpenTracing;

namespace MicroBoost.Jaeger.Core
{
    public class CustomScope : IScope
    {
        private IScope _scope;
        private TracingContext _tracingContext;
        public ISpan Span { get; private set; }

        public CustomScope(ISpanBuilder spanBuilder, TracingContext tracingContext)
        {
            if (tracingContext.RootSpanContext is not null && tracingContext.CurrentSpan is null)
            {
                spanBuilder.AsChildOf(tracingContext.RootSpanContext);
            }
            else if (tracingContext.CurrentSpan is not null)
            {
                spanBuilder.AsChildOf(tracingContext.CurrentSpan.Context);
            }

            BuildCustomScope(spanBuilder, tracingContext);
        }

        private void BuildCustomScope(ISpanBuilder spanBuilder, TracingContext tracingContext)
        {
            _scope = spanBuilder.StartActive(true);
            _tracingContext = tracingContext;
            Span = _scope.Span;

            tracingContext.AddSpan(Span);
        }

        public void Dispose()
        {
            _tracingContext.RevertSpan();
            _scope.Dispose();
        }
    }
}