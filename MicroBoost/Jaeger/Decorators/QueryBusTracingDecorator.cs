﻿using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using OpenTracing;

namespace MicroBoost.Jaeger.Decorators
{
    public class QueryBusTracingDecorator : IQueryBus
    {
        private readonly IQueryBus _queryBus;
        private readonly ITracer _tracer;

        public QueryBusTracingDecorator(IQueryBus queryBus, ITracer tracer)
        {
            _queryBus = queryBus;
            _tracer = tracer;
        }

        public async Task<TResponse> SendAsync<TResponse>(IQuery<TResponse> query,
            CancellationToken cancellationToken)
        {
            using var firstScope = _tracer.StartSpanScope($"Receive {query.GetType().Name} query");
            using var secondScope = _tracer.StartSpanScope($"Handle {query.GetType().Name} query");
            return await _queryBus.SendAsync(query, cancellationToken);
        }
    }
}