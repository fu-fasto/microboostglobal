﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;
using MicroBoost.Types;

namespace MicroBoost.Jaeger.Decorators
{
    public class RepositoryDecorator<TEntity, T> : IRepository<TEntity, T> where TEntity : class, IEntity<T>
    {
        private readonly IRepository<TEntity, T> _repository;
        private readonly PersistenceOptions _persistenceOptions;

        public RepositoryDecorator(IRepository<TEntity, T> repository, PersistenceOptions persistenceOptions)
        {
            _repository = repository;
            _persistenceOptions = persistenceOptions;
        }

        public async Task<bool> IsExistsAsync(Expression<Func<TEntity, bool>> filter,
            CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_persistenceOptions.StandardName);
            var scope = tracer.StartSpanScope("Is record exists?");
            var response = await _repository.IsExistsAsync(filter, cancellationToken);
            scope.Dispose();
            tracer.Dispose();
            return response;
        }

        public async Task<bool> IsExistsAsync(T id, CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_persistenceOptions.StandardName);
            var scope = tracer.StartSpanScope("Is record exists?");
            var response = await _repository.IsExistsAsync(id, cancellationToken);
            scope.Dispose();
            tracer.Dispose();
            
            return response;
        }

        public async Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> filter,
            CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_persistenceOptions.StandardName);
            var scope = tracer.StartSpanScope("Get one record");
            var response = await _repository.FindOneAsync(filter, cancellationToken);
            scope.Dispose();
            tracer.Dispose();
            
            return response;
        }

        public async Task<TEntity> FindOneAsync(T id, CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_persistenceOptions.StandardName);
            var scope = tracer.StartSpanScope("Get one record");
            var response = await _repository.FindOneAsync(id, cancellationToken);
            scope.Dispose();
            tracer.Dispose();
            
            return response;
        }

        public async Task<IEnumerable<TEntity>> FindAsync(IFindQuery<TEntity> findQuery,
            CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_persistenceOptions.StandardName);
            var scope = tracer.StartSpanScope("Get multiple records");
            var response = await _repository.FindAsync(findQuery, cancellationToken);
            scope.Dispose();
            tracer.Dispose();
            
            return response;
        }

        public async Task<PagedResult<TEntity>> BrowseAsync(
            IFindQuery<TEntity> findQuery,
            CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_persistenceOptions.StandardName);
            var scope = tracer.StartSpanScope("Browse multiple records");
            var response = await _repository.BrowseAsync(findQuery, cancellationToken);
            scope.Dispose();
            tracer.Dispose();
           
            return response;
        }

        public async Task AddAsync(TEntity entity, CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_persistenceOptions.StandardName);
            var scope = tracer.StartSpanScope("Add one record");
            await _repository.AddAsync(entity, cancellationToken);
            scope.Dispose();
            tracer.Dispose();
        }

        public async Task AddRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_persistenceOptions.StandardName);
            var scope = tracer.StartSpanScope("Add multiple records");
            await _repository.AddRangeAsync(entities, cancellationToken);
            scope.Dispose();
            tracer.Dispose();
        }

        public async Task UpdateAsync(TEntity entity, CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_persistenceOptions.StandardName);
            var scope = tracer.StartSpanScope("Update one record");
            await _repository.UpdateAsync(entity, cancellationToken);
            scope.Dispose();
            tracer.Dispose();
        }

        public async Task UpdateRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_persistenceOptions.StandardName);
            var scope = tracer.StartSpanScope("Update multiple records");
            await _repository.UpdateRangeAsync(entities, cancellationToken);
            scope.Dispose();
            tracer.Dispose();
        }

        public async Task DeleteOneAsync(T id, CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_persistenceOptions.StandardName);
            var scope = tracer.StartSpanScope("Delete one record");
            await _repository.DeleteOneAsync(id, cancellationToken);
            scope.Dispose();
            tracer.Dispose();
        }

        public async Task DeleteRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_persistenceOptions.StandardName);
            var scope = tracer.StartSpanScope("Delete multiple records");
            await _repository.DeleteRangeAsync(entities, cancellationToken);
            scope.Dispose();
            tracer.Dispose();
        }

        void IRepository<TEntity, T>.DisableCaching()
        {
            _repository.DisableCaching();
        }
    }
}