﻿using System;
using MicroBoost.Helpers;
using Microsoft.Extensions.Logging;
using OpenTracing.Tag;

namespace MicroBoost.Jaeger.Decorators
{
    public class LoggerTracingDecorator<T> : ILogger<T>
    {
        private readonly ILogger<T> _logger;

        public LoggerTracingDecorator(ILogger<T> logger)
        {
            _logger = logger;
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            return _logger.BeginScope(state);
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return _logger.IsEnabled(logLevel);
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception,
            Func<TState, Exception, string> formatter)
        {
            var span = MicroBoostHelpers.GetCurrentSpan();
            if (span is not null)
            {
                if (logLevel == LogLevel.Error || logLevel == LogLevel.Critical)
                {
                    span.SetTag(Tags.Error, true);
                }

                span.Log(state.ToString());
            }

            _logger.Log(logLevel, eventId, state, exception, formatter);
        }
    }
}