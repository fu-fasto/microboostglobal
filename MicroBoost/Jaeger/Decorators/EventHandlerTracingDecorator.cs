﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MicroBoost.Events;
using MicroBoost.MessageBroker;
using OpenTracing;

namespace MicroBoost.Jaeger.Decorators
{
    public class EventHandlerTracingDecorator<TEvent> : INotificationHandler<TEvent> where TEvent : IEvent
    {
        private readonly INotificationHandler<TEvent> _handler;
        private readonly ITracer _tracer;
        private readonly MessageBrokerOptions _messageBrokerOptions;

        public EventHandlerTracingDecorator(INotificationHandler<TEvent> handler, ITracer tracer,
            MessageBrokerOptions messageBrokerOptions)
        {
            _handler = handler;
            _tracer = tracer;
            _messageBrokerOptions = messageBrokerOptions;
        }

        public async Task Handle(TEvent @event, CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_messageBrokerOptions.Type);
            var messageQueueScope = tracer.StartSpanScope("Consume a message");
            var scope = _tracer.StartSpanScope($"Handle {@event.GetType().Name} event");
           
            await _handler.Handle(@event, cancellationToken);
            
            scope.Dispose();
            messageQueueScope.Dispose();
            tracer.Dispose();
        }
    }
}