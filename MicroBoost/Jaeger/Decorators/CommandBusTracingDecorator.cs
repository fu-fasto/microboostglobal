﻿using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Commands;
using OpenTracing;

namespace MicroBoost.Jaeger.Decorators
{
    public class CommandBusTracingDecorator : ICommandBus
    {
        private readonly ICommandBus _commandBus;
        private readonly ITracer _tracer;

        public CommandBusTracingDecorator(ICommandBus commandBus, ITracer tracer)
        {
            _commandBus = commandBus;
            _tracer = tracer;
        }

        public async Task SendAsync(ICommand command, CancellationToken cancellationToken)
        {
            var scope = _tracer.StartSpanScope($"Receive {command.GetType().Name} command");
            await _commandBus.SendAsync(command, cancellationToken);
            scope.Dispose();
        }
    }
}