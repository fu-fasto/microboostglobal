﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;
using OpenTracing;

namespace MicroBoost.Jaeger.Decorators
{
    public class MessageQueueCommandHandlerDecorator<TCommand> : INotificationHandler<TCommand>
        where TCommand : ICommand
    {
        private readonly INotificationHandler<TCommand> _handler;
        private readonly ITracer _tracer;
        private readonly MessageBrokerOptions _messageBrokerOptions;

        public MessageQueueCommandHandlerDecorator(
            INotificationHandler<TCommand> handler,
            ITracer tracer,
            MessageBrokerOptions messageBrokerOptions)
        {
            _handler = handler;
            _tracer = tracer;
            _messageBrokerOptions = messageBrokerOptions;
        }

        public async Task Handle(TCommand command, CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_messageBrokerOptions.Type);
            var messageQueueScope = tracer.StartSpanScope("Consume a message");
            var scope = _tracer.StartSpanScope($"Handle {command.GetType().Name} command");
           
            await _handler.Handle(command, cancellationToken);
            
            scope.Dispose();
            messageQueueScope.Dispose();
            tracer.Dispose();
        }
    }
}