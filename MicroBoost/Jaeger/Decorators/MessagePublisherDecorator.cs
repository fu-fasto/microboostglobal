﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.MessageBroker;
using MicroBoost.Persistence.OutBox;
using OpenTracing;

namespace MicroBoost.Jaeger.Decorators
{
    public class MessagePublisherDecorator : IMessagePublisher
    {
        private readonly IMessagePublisher _messagePublisher;
        private readonly ITracer _tracer;
        private readonly MessageBrokerOptions _messageBrokerOptions;

        public MessagePublisherDecorator(IMessagePublisher messagePublisher, ITracer tracer,
            MessageBrokerOptions messageBrokerOptions)
        {
            _messagePublisher = messagePublisher;
            _tracer = tracer;
            _messageBrokerOptions = messageBrokerOptions;
        }

        public async Task PublishAsync(IMessage message, CancellationToken cancellationToken)
        {
            var tracer = JaegerExtensions.InitTracer(_messageBrokerOptions.Type);
            var messageQueueScope = tracer.StartSpanScope("Publish message to queue");
            
            await _messagePublisher.PublishAsync(message, cancellationToken);
            
            messageQueueScope.Dispose();
            tracer.Dispose();
        }

        public async Task PublishAsync(IEnumerable<OutBoxMessage> outBoxMessages,
            CancellationToken cancellationToken)
        {
            // var tracer = JaegerExtensions.InitTracer(_messageBrokerOptions.Type);
            // var messageQueueScope = tracer
            //     .StartSpanScope("Publish from outbox to queue");
            await _messagePublisher.PublishAsync(outBoxMessages, cancellationToken);
            // messageQueueScope.Dispose();
            // tracer.Dispose();
        }

        public async Task PublishOutBoxAsync(IMessage message, CancellationToken cancellationToken)
        {
            var scope = _tracer.StartSpanScope("Save message to outbox tables");
            await _messagePublisher.PublishOutBoxAsync(message, cancellationToken);
            scope.Dispose();
        }
    }
}