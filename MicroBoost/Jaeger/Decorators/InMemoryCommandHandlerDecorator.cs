﻿using System.Threading;
using System.Threading.Tasks;
using MediatR;
using MicroBoost.Cqrs.Commands;
using OpenTracing;

namespace MicroBoost.Jaeger.Decorators
{
    public class InMemoryCommandHandlerDecorator<TCommand> : INotificationHandler<TCommand>
        where TCommand : ICommand
    {
        private readonly INotificationHandler<TCommand> _handler;
        private readonly ITracer _tracer;

        public InMemoryCommandHandlerDecorator(
            INotificationHandler<TCommand> handler,
            ITracer tracer)
        {
            _handler = handler;
            _tracer = tracer;
        }

        public async Task Handle(TCommand command, CancellationToken cancellationToken)
        {
            var scope = _tracer.StartSpanScope($"Handle {command.GetType().Name} command");
            await _handler.Handle(command, cancellationToken);
            scope.Dispose();
        }
    }
}