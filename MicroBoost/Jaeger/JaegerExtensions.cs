using Jaeger;
using Jaeger.Reporters;
using Jaeger.Samplers;
using Jaeger.Senders.Thrift;
using MediatR;
using MicroBoost.Cqrs;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Jaeger.Core;
using MicroBoost.Jaeger.Decorators;
using MicroBoost.MessageBroker;
using MicroBoost.Persistence;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using OpenTracing;
using SimpleInjector;

namespace MicroBoost.Jaeger
{
    public static class JaegerExtensions
    {
        private const string SectionName = "Jaeger";
        private static Container _container;
        private static JaegerOptions _jaegerOptions = new();

        public static IMicroBoostBuilder AddJaeger(this IMicroBoostBuilder builder)
        {
            _jaegerOptions = builder.GetOptions<JaegerOptions>(SectionName);
            _jaegerOptions.Enabled = true;
            if (!_jaegerOptions.Enabled) return builder;

            _container = builder.Container;
            _container.Register<TracingContext>();
            _container.RegisterDecorator(typeof(ILogger<>), typeof(LoggerTracingDecorator<>), Lifestyle.Singleton);

            builder.Services.AddSingleton<ITracer>(_ =>
            {
                var appOptions = _container.GetRequiredService<AppOptions>();

                return InitTracer(appOptions.Name);
            });

            return builder;
        }

        public static IApplicationBuilder UseJaeger(this IApplicationBuilder app)
        {
            app.UseMiddleware<TracingMiddleware>();

            return app;
        }

        public static IScope StartSpanScope(this ITracer tracer, string name)
        {
            var tracingContext = _container.GetRequiredService<TracingContext>();
            var spanBuilder = tracer.BuildSpan(name);

            return new CustomScope(spanBuilder, tracingContext);
        }

        internal static Tracer InitTracer(string serviceName)
        {
            var loggerFactory = _container.GetRequiredService<ILoggerFactory>();
            var maxPacketSize = _jaegerOptions.MaxPacketSize <= 0 ? 64967 : _jaegerOptions.MaxPacketSize;

            var sender = new UdpSender(_jaegerOptions.UdpHost, _jaegerOptions.UdpPort, maxPacketSize);
            var reporter = new RemoteReporter.Builder()
                .WithSender(sender)
                .WithLoggerFactory(loggerFactory)
                .Build();

            return new Tracer.Builder(serviceName)
                .WithLoggerFactory(loggerFactory)
                .WithReporter(reporter)
                .WithSampler(GetSampler(_jaegerOptions))
                .Build();
        }

        internal static void AddRepositoryDecorator()
        {
            if (!_jaegerOptions.Enabled) return;

            _container.RegisterDecorator(typeof(IRepository<,>), typeof(RepositoryDecorator<,>));
        }

        internal static void AddCqrsDecorator(CqrsOptions cqrsOptions)
        {
            if (!_jaegerOptions.Enabled) return;

            _container.RegisterDecorator<IQueryBus, QueryBusTracingDecorator>();
            _container.RegisterDecorator<ICommandBus, CommandBusTracingDecorator>();

            if (cqrsOptions.UseMessageQueueCommand)
            {
                _container.RegisterDecorator(typeof(INotificationHandler<>),
                    typeof(MessageQueueCommandHandlerDecorator<>));
            }
            else
            {
                _container.RegisterDecorator(typeof(INotificationHandler<>),
                    typeof(InMemoryCommandHandlerDecorator<>));
            }
        }

        internal static void AddMessageBrokerDecorator()
        {
            if (!_jaegerOptions.Enabled) return;

            _container.RegisterDecorator<IMessagePublisher, MessagePublisherDecorator>(Lifestyle.Singleton);
            _container.RegisterDecorator(typeof(INotificationHandler<>), typeof(EventHandlerTracingDecorator<>));
        }

        private static ISampler GetSampler(JaegerOptions options)
        {
            switch (options.Sampler)
            {
                case "const": return new ConstSampler(true);
                case "rate": return new RateLimitingSampler(options.MaxTracesPerSecond);
                case "probabilistic": return new ProbabilisticSampler(options.SamplingRate);
                default: return new ConstSampler(true);
            }
        }
    }
}