﻿using System;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using SimpleInjector;

namespace MicroBoost
{
    public static class Extensions
    {
        private const string SectionName = "App";

        public static IMicroBoostBuilder AddMicroBoostBuilder(this IServiceCollection services,
            string sectionName = SectionName)
        {
            if (string.IsNullOrWhiteSpace(sectionName))
            {
                sectionName = SectionName;
            }

            var builder = MicroBoostBuilder.Create(services, Assembly.GetCallingAssembly());
            var options = builder.GetOptions<AppOptions>(sectionName);
            builder.Services.AddMemoryCache();

            services.AddSingleton<IMicroBoostBuilder>(builder);
            services.AddSingleton(options);

            if (!options.DisplayBanner || string.IsNullOrWhiteSpace(options.Name))
            {
                return builder;
            }

            var version = options.DisplayVersion ? $" {options.Version}" : string.Empty;
            Console.WriteLine(Figgle.FiggleFonts.Doom.Render($"{options.Name}{version}"));

            return builder;
        }

        public static TModel GetOptions<TModel>(this IConfiguration configuration, string sectionName)
            where TModel : new()
        {
            var model = new TModel();
            configuration.GetSection(sectionName).Bind(model);
            return model;
        }

        public static TModel GetOptions<TModel>(this IMicroBoostBuilder builder, string settingsSectionName)
            where TModel : class, new()
        {
            using var serviceProvider = builder.Services.BuildServiceProvider();
            var configuration = serviceProvider.GetService<IConfiguration>();
            var options = configuration.GetOptions<TModel>(settingsSectionName);
            builder.Services.TryAddSingleton(options);
            return options;
        }
        
        public static IMicroBoostBuilder RegisterOptions<TModel>(this IMicroBoostBuilder builder, string settingsSectionName)
            where TModel : class, new()
        {
            using var serviceProvider = builder.Services.BuildServiceProvider();
            var configuration = serviceProvider.GetService<IConfiguration>();
            var options = configuration.GetOptions<TModel>(settingsSectionName);
            builder.Services.TryAddSingleton(options);
            return builder;
        }

        public static IMicroBoostBuilder AddSingleton<TImplementation>(this IMicroBoostBuilder builder)
            where TImplementation : class
        {
            builder.Container.RegisterSingleton<TImplementation>();
            return builder;
        }
        
        public static IMicroBoostBuilder AddScoped<TImplementation>(this IMicroBoostBuilder builder)
            where TImplementation : class
        {
            builder.Container.Register<TImplementation>(Lifestyle.Scoped);
            return builder;
        }
        
        public static IMicroBoostBuilder AddTransient<TImplementation>(this IMicroBoostBuilder builder)
            where TImplementation : class
        {
            builder.Container.Register<TImplementation>(Lifestyle.Transient);
            return builder;
        }
        
        public static IMicroBoostBuilder AddSingleton<TService, TImplementation>(this IMicroBoostBuilder builder)
            where TService : class
            where TImplementation : class, TService
        {
            builder.Container.RegisterSingleton<TService, TImplementation>();
            return builder;
        }
        
        public static IMicroBoostBuilder AddScoped<TService, TImplementation>(this IMicroBoostBuilder builder)
            where TService : class
            where TImplementation : class, TService
        {
            builder.Container.Register<TService, TImplementation>(Lifestyle.Scoped);
            return builder;
        }

        public static IMicroBoostBuilder AddTransient<TService, TImplementation>(this IMicroBoostBuilder builder)
            where TService : class
            where TImplementation : class, TService
        {
            builder.Container.Register<TService, TImplementation>(Lifestyle.Transient);
            return builder;
        }
    }
}