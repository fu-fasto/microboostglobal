﻿namespace MicroBoost.Types
{
    public abstract class EntityBase<T> : IEntity<T>
    {
        public virtual T Id { get; set; }
    }
}
