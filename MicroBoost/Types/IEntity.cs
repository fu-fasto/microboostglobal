﻿namespace MicroBoost.Types
{
    public interface IEntity<T>
    {
        T Id { get; set; }
    }
}
