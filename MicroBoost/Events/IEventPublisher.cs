﻿using System.Threading;
using System.Threading.Tasks;

namespace MicroBoost.Events
{
    public interface IEventPublisher
    {
        Task PublishAsync(IEvent @event, CancellationToken cancellationToken = default);
    }
}
