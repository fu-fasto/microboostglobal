﻿using MediatR;
using MicroBoost.MessageBroker;

namespace MicroBoost.Events
{
    public interface IEvent : IMessage, INotification
    {
    }
}
