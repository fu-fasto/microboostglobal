﻿using System.Threading;
using System.Threading.Tasks;

namespace MicroBoost.Http
{
    public interface IHttpClient
    {
        Task<TResponse> GetAsync<TResponse>(string path, CancellationToken cancellationToken = default);

        Task<TResponse> PostAsync<TRequest, TResponse>(string path, TRequest body,
            CancellationToken cancellationToken = default);
        
        Task<TResponse> PutAsync<TRequest, TResponse>(string path, TRequest body,
            CancellationToken cancellationToken = default);
        
        Task<TResponse> PatchAsync<TRequest, TResponse>(string path, TRequest body,
            CancellationToken cancellationToken = default);
    }
}