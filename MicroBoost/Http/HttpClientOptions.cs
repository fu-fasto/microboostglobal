﻿using System.Collections.Generic;

namespace MicroBoost.Http
{
    public class HttpClientOptions
    {
        public int Retries { get; set; }
        public IDictionary<string, string> Services { get; set; }
    }
}