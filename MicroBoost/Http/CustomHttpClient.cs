﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Helpers;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Polly;

namespace MicroBoost.Http
{
    public class CustomHttpClient : IHttpClient
    {
        private readonly ILogger<CustomHttpClient> _logger;
        private readonly HttpClient _httpClient;
        private readonly HttpClientOptions _httpClientOptions;

        public CustomHttpClient(HttpClientOptions httpClientOptions, IHttpClientFactory clientFactory,
            ILogger<CustomHttpClient> logger)
        {
            _httpClientOptions = httpClientOptions;
            _logger = logger;
            _httpClient = clientFactory.CreateClient();
        }

        public Task<T> GetAsync<T>(string path, CancellationToken cancellationToken = default)
        {
            return SendAsync<T>(CreateRequestMessage(path, HttpMethod.Get));
        }

        public Task<TResponse> PostAsync<TRequest, TResponse>(string path, TRequest body, CancellationToken cancellationToken = default)
        {
            return SendAsync<TResponse>(CreateRequestMessage(path, HttpMethod.Post, body));
        }

        public Task<TResponse> PutAsync<TRequest, TResponse>(string path, TRequest body, CancellationToken cancellationToken = default)
        {
            return SendAsync<TResponse>(CreateRequestMessage(path, HttpMethod.Put, body));
        }

        public Task<TResponse> PatchAsync<TRequest, TResponse>(string path, TRequest body, CancellationToken cancellationToken = default)
        {
            return SendAsync<TResponse>(CreateRequestMessage(path, HttpMethod.Patch, body));
        }

        private Task<T> SendAsync<T>(HttpRequestMessage request)
        {
            return Policy
                .Handle<Exception>(e => throw new Exception($"{request.RequestUri?.AbsoluteUri} - {e.Message}"))
                .WaitAndRetryAsync(_httpClientOptions.Retries, r => TimeSpan.FromMilliseconds(500))
                .ExecuteAsync(async () =>
                {
                    var response = await _httpClient.SendAsync(request);

                    if (!response.IsSuccessStatusCode)
                        switch (Convert.ToInt32(response.StatusCode))
                        {
                            case >=500:
                                throw new Exception($"{request.RequestUri?.AbsoluteUri} - internal server error!!!");
                            case >=400:
                                return default;
                        }

                    var text = await response.Content.ReadAsStringAsync();
                    try
                    {
                        return JsonConvert.DeserializeObject<T>(text);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("{Error}", e.Message);
                        return default;
                    }
                });
        }

        private HttpRequestMessage CreateRequestMessage(string path, HttpMethod method, object content = null)
        {
            var request = new HttpRequestMessage();
            var uri = path.StartsWith("http") ? path : $"http://{path}";

            InjectTracingHeader(request);
            request.Method = method;
            request.RequestUri = new Uri(uri);
            if (content is not null)
            {
                request.Content = new StringContent(JsonConvert.SerializeObject(content),
                    Encoding.UTF8,
                    "application/json");
            }

            return request;
        }

        private void InjectTracingHeader(HttpRequestMessage request)
        {
            var currentSpan = MicroBoostHelpers.GetCurrentSpan();
            if (currentSpan is null) return;

            var tracingHeader = currentSpan.Context.ToString();
            request.Headers.Add("x-root-span-context", tracingHeader);
        }
    }
}