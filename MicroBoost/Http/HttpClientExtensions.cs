﻿using Microsoft.Extensions.DependencyInjection;

namespace MicroBoost.Http
{
    public static class HttpClientExtensions
    {
        private const string SectionName = "HttpClient";

        public static IMicroBoostBuilder AddHttpClient(this IMicroBoostBuilder builder)
        {
            _ = builder.GetOptions<HttpClientOptions>(SectionName);
            builder.Container.RegisterSingleton<IHttpClient, CustomHttpClient>();
            builder.Services.AddHttpClient();
            
            return builder;
        }
    }
}