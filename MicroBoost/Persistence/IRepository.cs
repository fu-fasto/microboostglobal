﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Types;

namespace MicroBoost.Persistence
{
    public interface IRepository<TEntity, T> where TEntity : IEntity<T>
    {
        Task<bool> IsExistsAsync(Expression<Func<TEntity, bool>> filter, CancellationToken cancellationToken = default);

        Task<bool> IsExistsAsync(T id, CancellationToken cancellationToken = default);

        Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> filter,
            CancellationToken cancellationToken = default);

        Task<TEntity> FindOneAsync(T id, CancellationToken cancellationToken = default);

        Task<IEnumerable<TEntity>> FindAsync(IFindQuery<TEntity> findQuery = default(FindQuery<TEntity>),
            CancellationToken cancellationToken = default);

        Task<PagedResult<TEntity>> BrowseAsync(IFindQuery<TEntity> findQuery = default(FindQuery<TEntity>),
            CancellationToken cancellationToken = default);

        Task AddAsync(TEntity entity, CancellationToken cancellationToken = default);

        Task AddRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default);

        Task UpdateAsync(TEntity entity, CancellationToken cancellationToken = default);
        
        Task UpdateRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default);

        Task DeleteOneAsync(T id, CancellationToken cancellationToken = default);
        
        Task DeleteRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default);

        internal void DisableCaching();
    }
}