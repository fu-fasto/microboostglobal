﻿using System;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using MySqlConnector;

namespace MicroBoost.Persistence.EfCore
{
    public static class EfCoreExtensions
    {
        internal static IMicroBoostBuilder AddEfCore<TContext>
            (this IMicroBoostBuilder builder, PersistenceOptions persistenceOptions)
            where TContext : DbContext
        {
            EfCoreOptions.DbContextType = typeof(TContext);

            switch (persistenceOptions.DatabaseType.ToLower())
            {
                case "mysql":
                    persistenceOptions.StandardName = "MySQL";
                    builder.Services.AddDbContextPool<TContext>(options =>
                    {
                        options.UseMySql(
                            CreateMySqlConnection(persistenceOptions),
                            new MySqlServerVersion(new Version(persistenceOptions.DatabaseVersion)), o =>
                            {
                                if (!persistenceOptions.DisableRetryOnFailure)
                                {
                                    o.EnableRetryOnFailure(10, TimeSpan.FromSeconds(10), null);
                                }
                            });
                    }, 8);
                    break;

                case "sqlserver":
                case "mssql":
                    persistenceOptions.StandardName = "SQL Server";
                    builder.Services.AddDbContextPool<TContext>(options =>
                    {
                        options.UseSqlServer(CreateSqlServerConnection(persistenceOptions));
                    });
                    break;
            }

            builder.Container.Register(typeof(IRepository<,>), typeof(EfCoreRepository<,>));
            builder.Container.Register<IUnitOfWork, EfCoreUnitOfWork>();

            return builder;
        }

        private static string CreateSqlServerConnection(PersistenceOptions persistenceOptions)
        {
            var builder = new SqlConnectionStringBuilder
            {
                Authentication = SqlAuthenticationMethod.SqlPassword,
                DataSource = persistenceOptions.DatabaseHost,
                UserID = persistenceOptions.Username,
                Password = persistenceOptions.Password,
                Pooling = true,
                MinPoolSize = 4,
                MaxPoolSize = 8,
                ConnectTimeout = 10
            };

            return builder.ToString();
        }

        private static string CreateMySqlConnection(PersistenceOptions persistenceOptions)
        {
            var builder = new MySqlConnectionStringBuilder
            {
                Server = persistenceOptions.DatabaseHost,
                Database = persistenceOptions.DatabaseName,
                UserID = persistenceOptions.Username,
                Password = persistenceOptions.Password,
                Pooling = true,
                MaximumPoolSize = 8,
                MinimumPoolSize = 4
            };

            return builder.ToString();
        }
    }
}