﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Types;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SimpleInjector;

namespace MicroBoost.Persistence.EfCore
{
    public class EfCoreRepository<TEntity, T> : IRepository<TEntity, T> where TEntity : class, IEntity<T>
    {
        protected readonly DbContext DbContext;

        public EfCoreRepository(Container container)
        {
            DbContext = (DbContext) container.GetRequiredService(EfCoreOptions.DbContextType);
        }

        public async Task<IEnumerable<TEntity>> FindAsync(
            IFindQuery<TEntity> findQuery,
            CancellationToken cancellationToken)
        {
            findQuery ??= new FindQuery<TEntity>();
            IQueryable<TEntity> queryable = DbContext.Set<TEntity>().AsQueryable();
            foreach (var filter in findQuery.Filters)
            {
                queryable = queryable.Where(filter);
            }

            return await queryable.ToListAsync(cancellationToken);
        }

        public async Task<PagedResult<TEntity>> BrowseAsync(
            IFindQuery<TEntity> findQuery = default(FindQuery<TEntity>),
            CancellationToken cancellationToken = default)
        {
            findQuery ??= new FindQuery<TEntity>();
            IQueryable<TEntity> queryable = DbContext.Set<TEntity>().AsQueryable();
            foreach (var filter in findQuery.Filters)
            {
                queryable = queryable.Where(filter);
            }

            if (!findQuery.SortExpressions.Any())
                queryable = queryable.OrderBy(x => x.Id);
            else
                foreach (var orderBy in findQuery.SortExpressions)
                    queryable = orderBy.IsAscending
                        ? queryable.OrderBy(orderBy.Selector)
                        : queryable.OrderByDescending(orderBy.Selector);

            var queryableItems = queryable
                .Skip(findQuery!.Page * findQuery!.Size)
                .Take(findQuery.Size);
            
            var items = await queryableItems!.ToListAsync(cancellationToken);
            var totals = await queryable.CountAsync(cancellationToken);

            return new PagedResult<TEntity>
            {
                CurrentPage = findQuery.Page,
                Items = items,
                TotalItems = totals,
                TotalPages = totals / findQuery.Size + (totals % findQuery.Size == 0 ? 0 : 1)
            };
        }

        public Task<bool> IsExistsAsync(Expression<Func<TEntity, bool>> filter,
            CancellationToken cancellationToken = default)
        {
            return DbContext.Set<TEntity>().AnyAsync(filter, cancellationToken);
        }

        public Task<bool> IsExistsAsync(T id, CancellationToken cancellationToken)
        {
            return DbContext.Set<TEntity>().Select(x => x.Id).ContainsAsync(id, cancellationToken);
        }

        public Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> filter,
            CancellationToken cancellationToken = default)
        {
            return DbContext.Set<TEntity>().FirstOrDefaultAsync(filter, cancellationToken);
        }

        public Task<TEntity> FindOneAsync(T id, CancellationToken cancellationToken)
        {
            return DbContext.Set<TEntity>().FirstOrDefaultAsync(x => x.Id.Equals(id), cancellationToken);
        }

        public virtual async Task AddAsync(TEntity entity, CancellationToken cancellationToken)
        {
            await DbContext.Set<TEntity>().AddAsync(entity, cancellationToken);
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task AddRangeAsync(IEnumerable<TEntity> entities,
            CancellationToken cancellationToken = default)
        {
            await DbContext.Set<TEntity>().AddRangeAsync(entities, cancellationToken);
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task UpdateAsync(TEntity entity, CancellationToken cancellationToken)
        {
            DbContext.Set<TEntity>().Update(entity);
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task UpdateRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default)
        {
            DbContext.UpdateRange(entities);
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        public virtual async Task DeleteOneAsync(T id, CancellationToken cancellationToken)
        {
            var entity = await DbContext.Set<TEntity>().FirstOrDefaultAsync(x => x.Id.Equals(id), cancellationToken);
            DbContext.Set<TEntity>().Remove(entity);
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task DeleteRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default)
        {
            DbContext.RemoveRange(entities);
            await DbContext.SaveChangesAsync(cancellationToken);
        }

        void IRepository<TEntity, T>.DisableCaching()
        {
        }
    }
}