﻿using System;

namespace MicroBoost.Persistence.EfCore
{
    public static class EfCoreOptions
    {
        public static Type DbContextType { get; set; }
    }
}
