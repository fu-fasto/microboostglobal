﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Storage;

namespace MicroBoost.Persistence.EfCore
{
    public class EfCoreTransaction : ITransaction
    {
        private readonly IDbContextTransaction _dbContextTransaction;
        private bool _isDone;
        
        public EfCoreTransaction(IDbContextTransaction dbContextTransaction)
        {
            _dbContextTransaction = dbContextTransaction;
        }

        public void Dispose()
        {
            if (!_isDone)
            {
                _dbContextTransaction.Rollback();
            }
            _dbContextTransaction?.Dispose();
        }

        public async Task CommitAsync(CancellationToken cancellationToken = default)
        {
            await _dbContextTransaction.CommitAsync(cancellationToken);
            _isDone = true;
        }

        public async Task RollbackAsync(CancellationToken cancellationToken = default)
        {
            await _dbContextTransaction.RollbackAsync(cancellationToken);
            _isDone = true;
        }
    }
}