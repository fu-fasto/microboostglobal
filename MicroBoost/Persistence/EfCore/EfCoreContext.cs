﻿using MicroBoost.Persistence.Caching;
using MicroBoost.Persistence.OutBox;
using Microsoft.EntityFrameworkCore;

namespace MicroBoost.Persistence.EfCore
{
    public class EfCoreContext : DbContext
    {
        protected DbSet<DbPartitionState> DbPartitionStates { get; set; }
        protected DbSet<OutBoxMessage> OutBoxMessages { get; set; }

        public EfCoreContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<DbPartitionState>().ToTable("__db_partition_states");
            modelBuilder.Entity<DbPartitionState>().Property(x => x.Id).HasColumnName("id");
            modelBuilder.Entity<DbPartitionState>().Property(x => x.Partition).HasColumnName("partition");
            modelBuilder.Entity<DbPartitionState>().Property(x => x.CommittedOffset).HasColumnName("committed_offset");
            
            modelBuilder.Entity<OutBoxMessage>().ToTable("__outbox_messages");
            modelBuilder.Entity<OutBoxMessage>().Property(x => x.Id).HasColumnName("id");
            modelBuilder.Entity<OutBoxMessage>().Property(x => x.Key).HasColumnName("key");
            modelBuilder.Entity<OutBoxMessage>().Property(x => x.Offset).HasColumnName("offset");
            modelBuilder.Entity<OutBoxMessage>().Property(x => x.Partition).HasColumnName("partition");
            modelBuilder.Entity<OutBoxMessage>().Property(x => x.Topic).HasColumnName("topic");
            modelBuilder.Entity<OutBoxMessage>().Property(x => x.Type).HasColumnName("type");
            modelBuilder.Entity<OutBoxMessage>().Property(x => x.Value).HasColumnName("value");
            modelBuilder.Entity<OutBoxMessage>().Property(x => x.CreatedTime).HasColumnName("created_time");
            modelBuilder.Entity<OutBoxMessage>().Property(x => x.IsSend).HasColumnName("is_send");
            modelBuilder.Entity<OutBoxMessage>().Property(x => x.SpanContext).HasColumnName("span_context");
        }
    }
}