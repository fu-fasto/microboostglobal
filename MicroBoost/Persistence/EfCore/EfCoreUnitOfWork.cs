﻿using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Types;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SimpleInjector;

namespace MicroBoost.Persistence.EfCore
{
    public class EfCoreUnitOfWork : IUnitOfWork
    {
        private readonly DbContext _dbContext;
        private readonly Container _container;

        public EfCoreUnitOfWork(Container container)
        {
            _container = container;
            _dbContext = (DbContext) container.GetRequiredService(EfCoreOptions.DbContextType);
        }

        public IRepository<TEntity, T> GetRepository<TEntity, T>() where TEntity : IEntity<T>
        {
            return (IRepository<TEntity, T>) _container.GetRequiredService(typeof(IRepository<TEntity, T>));
        }

        public async Task<ITransaction> BeginTransactionAsync(CancellationToken cancellationToken = default)
        {
            var transaction = await _dbContext.Database.BeginTransactionAsync(cancellationToken);
            return new EfCoreTransaction(transaction);
        }

        public async Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            await _dbContext.SaveChangesAsync(cancellationToken);
        }

        void IUnitOfWork.DisableCaching()
        {
        }
    }
}