﻿#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Helpers;
using MicroBoost.Types;
using Microsoft.Extensions.Hosting;
using MongoDB.Bson;
using MongoDB.Driver;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace MicroBoost.Persistence.Mongo
{
    [AttributeUsage(AttributeTargets.Property)]
    public class FtsIndex : Attribute
    {
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class UniqueIndex : Attribute
    {
    }


    [AttributeUsage(AttributeTargets.Property)]
    public class RawIndex : Attribute
    {
        public RawIndex(string name, string expression)
        {
            Name = name;
            Expression = expression;
        }

        public string Name { get; set; }
        public string Expression { get; set; }
    }

    internal class MongoBootstrapping : IHostedService
    {
        private readonly Container _container;
        private readonly Assembly _callingAssembly;

        public MongoBootstrapping(IMicroBoostBuilder builder)
        {
            _container = builder.Container;
            _callingAssembly = builder.CallingAssembly;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Task.Run(InitDatabase, cancellationToken);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void InitDatabase()
        {
            using var scope = AsyncScopedLifestyle.BeginScope(_container);
            var database = scope.GetInstance<IMongoDatabase>();

            var entityTypes = _callingAssembly.GetTypesHaveGenericInterface(typeof(IEntity<>));
            entityTypes.ForEach(entityType =>
            {
                typeof(MongoBootstrapping).GetMethod("InitCollection")!
                    .MakeGenericMethod(entityType)
                    .Invoke(this, new object?[] {database});
            });
        }

        public void InitCollection<TEntity>(IMongoDatabase database)
        {
            var collection = database.GetCollection<TEntity>(typeof(TEntity).Name);
            var existingIndexes = collection.Indexes.List().ToList();
            List<CreateIndexModel<TEntity>> newIndexes = new();

            // Get Full-text search index
            var ftsProperties = typeof(TEntity).GetPropertiesInfosHasAttribute(typeof(FtsIndex));
            if (ftsProperties.Any())
            {
                string indexKeysName = typeof(TEntity).Name;
                var indexKeysDefinitionBuilder = new IndexKeysDefinitionBuilder<TEntity>();
                var indexKeys = ftsProperties.Select(propertyInfo =>
                {
                    indexKeysName += "_" + propertyInfo.Name;
                    return indexKeysDefinitionBuilder.Text(propertyInfo.Name);
                });
                var indexKeysDefinition = indexKeysDefinitionBuilder.Combine(indexKeys);

                var createIndexModel = new CreateIndexModel<TEntity>(indexKeysDefinition, new CreateIndexOptions()
                {
                    Name = $"{indexKeysName}"
                });

                newIndexes.Add(createIndexModel);
            }

            // Get Unique index
            var uniqueProperties = typeof(TEntity).GetPropertiesInfosHasAttribute(typeof(UniqueIndex));
            foreach (var propertyInfo in uniqueProperties)
            {
                string indexKeysName = $"{typeof(TEntity).Name}_{propertyInfo.Name}";
                var indexKeysDefinition = new IndexKeysDefinitionBuilder<TEntity>().Ascending(propertyInfo.Name);
                var createIndexModel = new CreateIndexModel<TEntity>(indexKeysDefinition, new CreateIndexOptions
                {
                    Name = $"{indexKeysName}"
                });

                newIndexes.Add(createIndexModel);
            }

            // Get raw index
            var rawProperties = typeof(TEntity).GetPropertiesInfosHasAttribute(typeof(RawIndex));
            foreach (var propertyInfo in rawProperties)
            {
                var rawIndex = propertyInfo.GetCustomAttribute<RawIndex>();
                var indexKeysDefinition =
                    new BsonDocumentIndexKeysDefinition<TEntity>(new BsonDocument() {{rawIndex.Expression, 1}});
                var createIndexModel = new CreateIndexModel<TEntity>(indexKeysDefinition, new CreateIndexOptions
                {
                    Name = $"{rawIndex.Name}"
                });
                
                newIndexes.Add(createIndexModel);
            }

            // Drop existingIndexes not appear in models
            foreach (var existingIndex in existingIndexes)
            {
                var indexName = existingIndex.GetElement(2).Value.ToString();
                if (indexName == "_id_" ||
                    newIndexes.Exists(newIndex => indexName == newIndex.Options.Name)) continue;

                collection.Indexes.DropOne(indexName);
            }

            // Create index not appear in oldModels
            foreach (var newIndex in newIndexes)
            {
                var indexName = newIndex.Options.Name;
                if (existingIndexes.Exists(existingIndex =>
                    indexName == existingIndex.GetElement(2).Value.ToString())) continue;

                collection.Indexes.CreateOne(newIndex);
            }
        }
    }
}