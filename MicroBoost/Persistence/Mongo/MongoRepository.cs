﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence.Caching;
using MicroBoost.Types;
using MongoDB.Driver;

namespace MicroBoost.Persistence.Mongo
{
    public class MongoRepository<TEntity, T> : IRepository<TEntity, T> where TEntity : class, IEntity<T>
    {
        protected readonly IMongoCollection<TEntity> Collection;
        private readonly IUnitOfWork _unitOfWork;

        public MongoRepository(IMongoDatabase database, IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            Collection = database.GetCollection<TEntity>(typeof(TEntity).Name);
            Collection = Collection.WithReadPreference(ReadPreference.SecondaryPreferred);
        }

        public Task<bool> IsExistsAsync(Expression<Func<TEntity, bool>> filter,
            CancellationToken cancellationToken = default)
        {
            return Collection.Find(filter).AnyAsync(cancellationToken);
        }

        public Task<bool> IsExistsAsync(T id, CancellationToken cancellationToken)
        {
            return Collection.Find(e => e.Id.Equals(id)).AnyAsync(cancellationToken);
        }

        public Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> filter,
            CancellationToken cancellationToken = default)
        {
            return Collection.Find(filter).SingleOrDefaultAsync(cancellationToken);
        }

        public Task<TEntity> FindOneAsync(T id, CancellationToken cancellationToken)
        {
            return Collection.Find(e => e.Id.Equals(id)).SingleOrDefaultAsync(cancellationToken);
        }

        public async Task<IEnumerable<TEntity>> FindAsync(
            IFindQuery<TEntity> findQuery,
            CancellationToken cancellationToken)
        {
            findQuery ??= new FindQuery<TEntity>();
            SortDefinition<TEntity> sortDefinition;

            var filterDefinition = Builders<TEntity>.Filter.Empty;
            foreach (var filter in findQuery.Filters)
            {
                filterDefinition &= Builders<TEntity>.Filter.Where(filter);
            }

            if (!String.IsNullOrWhiteSpace(findQuery.Text))
            {
                filterDefinition &= Builders<TEntity>.Filter.Text(findQuery.Text);
            }

            if (findQuery.SortExpression is null)
            {
                sortDefinition = Builders<TEntity>.Sort.Ascending(x => x.Id);
            }
            else
            {
                sortDefinition = findQuery.SortExpression.IsAscending
                    ? Builders<TEntity>.Sort.Ascending(findQuery.SortExpression.Selector)
                    : Builders<TEntity>.Sort.Descending(findQuery.SortExpression.Selector);
            }

            return await Collection
                .Find(filterDefinition)
                .Sort(sortDefinition)
                .Skip(findQuery!.Page * findQuery!.Size)
                .Limit(findQuery.Size)
                .ToListAsync(cancellationToken);
        }

        public async Task<PagedResult<TEntity>> BrowseAsync(
            IFindQuery<TEntity> findQuery,
            CancellationToken cancellationToken)
        {
            findQuery ??= new FindQuery<TEntity>();
            SortDefinition<TEntity> sortDefinition;

            var filterDefinition = Builders<TEntity>.Filter.Empty;
            foreach (var filter in findQuery.Filters)
            {
                filterDefinition &= Builders<TEntity>.Filter.Where(filter);
            }

            if (!String.IsNullOrWhiteSpace(findQuery.Text))
            {
                filterDefinition &= Builders<TEntity>.Filter.Text(findQuery.Text);
            }

            if (findQuery.SortExpression is null)
            {
                sortDefinition = Builders<TEntity>.Sort.Ascending(x => x.Id);
            }
            else
            {
                sortDefinition = findQuery.SortExpression.IsAscending
                    ? Builders<TEntity>.Sort.Ascending(findQuery.SortExpression.Selector)
                    : Builders<TEntity>.Sort.Descending(findQuery.SortExpression.Selector);
            }

            var items = await Collection
                .Find(filterDefinition)
                .Sort(sortDefinition)
                .Skip(findQuery!.Page * findQuery!.Size)
                .Limit(findQuery.Size)
                .ToListAsync(cancellationToken);

            var totals = await Collection.CountDocumentsAsync(filterDefinition, null, cancellationToken);

            return new PagedResult<TEntity>
            {
                CurrentPage = findQuery.Page,
                Items = items,
                TotalItems = totals,
                TotalPages = (int) totals / findQuery.Size + (totals % findQuery.Size == 0 ? 0 : 1)
            };
        }

        public virtual Task AddAsync(TEntity entity, CancellationToken cancellationToken)
        {
            var session = GetSessionHandle();
            if (session is null)
                return Collection.InsertOneAsync(entity, null, cancellationToken);

            return Collection.InsertOneAsync(session, entity, null, cancellationToken);
        }

        public virtual Task AddRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken)
        {
            if (!entities.Any()) return Task.CompletedTask;

            var session = GetSessionHandle();
            if (session is null)
                return Collection.InsertManyAsync(entities, null, cancellationToken);

            return Collection.InsertManyAsync(session, entities, null, cancellationToken);
        }

        public virtual Task UpdateAsync(TEntity entity, CancellationToken cancellationToken)
        {
            var session = GetSessionHandle();
            if (session is null)
                return Collection.ReplaceOneAsync(e => e.Id.Equals(entity.Id), entity,
                    cancellationToken: cancellationToken);

            return Collection.ReplaceOneAsync(session, e => e.Id.Equals(entity.Id), entity,
                cancellationToken: cancellationToken);
        }

        public Task UpdateRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default)
        {
            if (!entities.Any()) return Task.CompletedTask;

            var writeModels = new List<WriteModel<TEntity>>();
            foreach (var entity in entities)
            {
                var filterDefinition = Builders<TEntity>.Filter.Eq(e => e.Id, entity.Id);
                writeModels.Add(new ReplaceOneModel<TEntity>(filterDefinition, entity));
            }

            var session = GetSessionHandle();
            if (session is null)
                return Collection.BulkWriteAsync(writeModels, null, cancellationToken);

            return Collection.BulkWriteAsync(session, writeModels, null, cancellationToken);
        }

        public virtual Task DeleteOneAsync(T id, CancellationToken cancellationToken)
        {
            var session = GetSessionHandle();
            if (session is null)
                return Collection.DeleteOneAsync(e => e.Id.Equals(id), cancellationToken);

            return Collection.DeleteOneAsync(session, e => e.Id.Equals(id), cancellationToken: cancellationToken);
        }

        public Task DeleteRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default)
        {
            if (!entities.Any()) return Task.CompletedTask;

            var writeModels = new List<WriteModel<TEntity>>();
            foreach (var entity in entities)
            {
                var filterDefinition = Builders<TEntity>.Filter.Eq(e => e.Id, entity.Id);
                writeModels.Add(new DeleteOneModel<TEntity>(filterDefinition));
            }

            var session = GetSessionHandle();
            if (session is null)
                return Collection.BulkWriteAsync(writeModels, null, cancellationToken);

            return Collection.BulkWriteAsync(session, writeModels, null, cancellationToken);
        }

        void IRepository<TEntity, T>.DisableCaching()
        {
        }

        private IClientSessionHandle GetSessionHandle()
        {
            if (_unitOfWork.GetType() == typeof(CachingUnitOfWork))
            {
                return null;
            }

            return ((MongoUnitOfWork) _unitOfWork).Session;
        }
    }
}