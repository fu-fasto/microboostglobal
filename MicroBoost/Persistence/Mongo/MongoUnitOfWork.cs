﻿using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Types;
using MongoDB.Driver;
using SimpleInjector;

namespace MicroBoost.Persistence.Mongo
{
    public class MongoUnitOfWork : IUnitOfWork
    {
        private readonly Container _container;
        private readonly IMongoClient _client;
        public IClientSessionHandle Session;

        public MongoUnitOfWork(IMongoClient client, Container container)
        {
            _client = client;
            _container = container;
        }

        public IRepository<TEntity, T> GetRepository<TEntity, T>() where TEntity : IEntity<T>
        {
            return (IRepository<TEntity, T>) _container.GetInstance(typeof(IRepository<TEntity, T>));
        }

        public async Task<ITransaction> BeginTransactionAsync(CancellationToken cancellationToken = default)
        {
            Session = await _client.StartSessionAsync(cancellationToken: cancellationToken);
            Session.StartTransaction();

            return new MongoTransaction(Session);
        }

        public Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }

        void IUnitOfWork.DisableCaching()
        {
        }
    }
}