﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using MongoDB.Driver;

namespace MicroBoost.Persistence.Mongo
{
    public static class MongoExtensions
    {
        internal static IMicroBoostBuilder AddMongo(this IMicroBoostBuilder builder,
            PersistenceOptions persistenceOptions)
        {
            persistenceOptions.StandardName = "MongoDB";
            builder.Container.Register<IMongoClient>(() =>
            {
                try
                {
                    var settings = new MongoClientSettings()
                    {
                        Server = new(persistenceOptions.DatabaseHost),
                        Credential =
                            MongoCredential.CreateCredential(
                                "admin",
                                persistenceOptions.Username,
                                persistenceOptions.Password
                            )
                    };
                    return new MongoClient(settings);
                }
                catch (Exception)
                {
                    var logger = builder.Container.GetRequiredService<ILogger<MongoUnitOfWork>>();
                    logger.LogCritical("MongoDB connection error!");
                    throw;
                }
            });
            builder.Container.Register(() =>
                builder.Container.GetRequiredService<IMongoClient>().GetDatabase(persistenceOptions.DatabaseName)
            );
            builder.Container.Register(typeof(IRepository<,>), typeof(MongoRepository<,>));
            builder.Container.Register<IUnitOfWork, MongoUnitOfWork>();
            builder.Services.AddHostedService<MongoBootstrapping>();

            return builder;
        }
    }
}