﻿using System.Threading;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace MicroBoost.Persistence.Mongo
{
    public class MongoTransaction : ITransaction
    {
        private IClientSessionHandle _session;
        
        public MongoTransaction(IClientSessionHandle session)
        {
            _session = session;
        }

        public void Dispose()
        {
            _session?.Dispose();
            _session = null;
        }

        public Task CommitAsync(CancellationToken cancellationToken = default)
        {
            return _session?.CommitTransactionAsync(cancellationToken);
        }

        public Task RollbackAsync(CancellationToken cancellationToken = default)
        {
            return _session?.AbortTransactionAsync(cancellationToken);
        }
    }
}