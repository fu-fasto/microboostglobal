﻿using System;
using MicroBoost.Types;

namespace MicroBoost.Persistence.OutBox
{
    public class OutBoxMessage : EntityBase<Guid>
    {
        public override Guid Id { get; set; } = Guid.NewGuid();
        public int Partition { get; set; }
        public long Offset { get; set; }
        public string Key { get; set; }
        public string Type { get; set; }
        public string SpanContext { get; set; }
        public string Topic { get; set; }
        public string Value { get; set; }
        public DateTimeOffset CreatedTime { get; set; } = DateTimeOffset.UtcNow;
        public bool IsSend { get; set; }
    }
}