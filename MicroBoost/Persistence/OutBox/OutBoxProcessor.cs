﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Jaeger;
using MicroBoost.MessageBroker;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace MicroBoost.Persistence.OutBox
{
    public class OutBoxProcessor : IHostedService
    {
        private static readonly Random Rand = new();
        private readonly Container _container;
        private readonly PersistenceOptions _options;
        private readonly ILogger<OutBoxProcessor> _logger;

        public OutBoxProcessor(Container container, ILogger<OutBoxProcessor> logger, PersistenceOptions options)
        {
            _container = container;
            _logger = logger;
            _options = options;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _ = Task.Run(async () =>
            {
                await Task.Delay(1000, cancellationToken);
                var findQuery = new FindQuery<OutBoxMessage>
                {
                    Page = 0,
                    Size = 1000,
                    SortExpressions = new List<SortExpression<OutBoxMessage>>
                    {
                        new()
                        {
                            Selector = x => x.CreatedTime,
                            IsAscending = false
                        }
                    }
                };
                findQuery.Filters.Add(x => x.IsSend == false);

                while (true)
                {
                    using (var scope = AsyncScopedLifestyle.BeginScope(_container))
                    {
                        try
                        {
                            var repository = scope.GetRequiredService<IRepository<OutBoxMessage, Guid>>();
                            var messagePublisher = scope.GetRequiredService<IMessagePublisher>();

                            var pagedResult = await repository.BrowseAsync(findQuery, cancellationToken);
                            var outBoxMessages = pagedResult.Items.ToList();

                            if (outBoxMessages.Count > 0)
                            {
                                var tracer = JaegerExtensions.InitTracer("Outbox");
                                var outboxScope = tracer?.StartSpanScope("Publish outbox messages to queue");

                                await messagePublisher.PublishAsync(outBoxMessages, cancellationToken);
                                foreach (var message in outBoxMessages)
                                {
                                    message.IsSend = true;
                                }

                                await repository.UpdateRangeAsync(outBoxMessages, cancellationToken);

                                outboxScope?.Dispose();
                                tracer?.Dispose();
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.LogError("{Message}", e.Message);
                        }
                    }

                    await Task.Delay(_options.OutBoxDelayTime + Rand.Next(100) * 10, cancellationToken);
                }
            }, cancellationToken);

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }
    }
}