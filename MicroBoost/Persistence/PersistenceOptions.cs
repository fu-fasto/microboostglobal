﻿namespace MicroBoost.Persistence
{
    public class PersistenceOptions
    {
        public string StandardName { get; set; }
        public string DatabaseType { get; set; }
        public string DatabaseVersion { get; set; }
        public string DatabaseName { get; set; }
        public string DatabaseHost { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public bool Caching { get; set; }
        public bool OutBox { get; set; }
        public int OutBoxDelayTime { get; set; } = 100;
        public int CachingDelayTime { get; set; } = 500;
        public bool Tracing { get; set; } = true;
        public bool DisableRetryOnFailure { get; set; }
    }
}