﻿using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Types;

namespace MicroBoost.Persistence
{
    public interface IUnitOfWork
    {
        IRepository<TEntity, TId> GetRepository<TEntity, TId>() where TEntity : IEntity<TId>;
        Task<ITransaction> BeginTransactionAsync(CancellationToken cancellationToken = default);
        Task SaveChangesAsync(CancellationToken cancellationToken = default);
        internal void DisableCaching();
    }
}
