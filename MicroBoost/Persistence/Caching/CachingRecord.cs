﻿using MicroBoost.Types;

namespace MicroBoost.Persistence.Caching
{
    public class CachingRecord<TEntity, TId> where TEntity : IEntity<TId>
    {
        public int Partition { get; set; }
        public long Offset { get; set; }
        public CachingRecordState State { get; set; }
        public TEntity Entity { get; set; }
    }

    public enum CachingRecordState
    {
        Inserted,
        Modified,
        Deleted,
        Normal
    }
}