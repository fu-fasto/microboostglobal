﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Types;
using Microsoft.Extensions.DependencyInjection;
using SimpleInjector;

namespace MicroBoost.Persistence.Caching
{
    public class CachingUnitOfWork : IUnitOfWork
    {
        readonly IUnitOfWork _originalUoW;
        private readonly Container _container;
        private bool _isEnabled = true;
        public CachingTransaction CachingTransaction;
        private readonly BlockingCollection<Action> _commitTransactionTasks = new();

        public CachingUnitOfWork(Container container, IUnitOfWork originalUoW)
        {
            _container = container;
            _originalUoW = originalUoW;
        }

        public IRepository<TEntity, T> GetRepository<TEntity, T>() where TEntity : IEntity<T>
        {
            return (IRepository<TEntity, T>) _container.GetRequiredService(typeof(IRepository<TEntity, T>));
        }

        public Task<ITransaction> BeginTransactionAsync(CancellationToken cancellationToken = default)
        {
            if (_isEnabled)
            {
                CachingTransaction = new CachingTransaction(this);
                return Task.FromResult((ITransaction) CachingTransaction);
            }
            
            return _originalUoW.BeginTransactionAsync(cancellationToken);
        }

        public Task SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            if (_isEnabled)
            {
                return Task.CompletedTask;
            }

            return _originalUoW.SaveChangesAsync(cancellationToken);
        }

        void IUnitOfWork.DisableCaching()
        {
            _isEnabled = false;
        }

        public void AddCommitTransactionTask(Action action)
        {
            _commitTransactionTasks.Add(action);
        }

        public void CommitTransaction()
        {
            foreach (var action in _commitTransactionTasks)
            {
                action.Invoke();
            }
        }
    }
}