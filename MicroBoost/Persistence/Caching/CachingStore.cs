﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.MessageBroker;
using MicroBoost.Types;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SimpleInjector;

namespace MicroBoost.Persistence.Caching
{
    public class CachingStore
    {
        private readonly BlockingCollection<Func<ICollection<PartitionState>, CancellationToken, Task>>
            _persistCollectionTasks = new();

        private readonly BlockingCollection<Action> _clearTasks = new();

        private readonly Container _container;
        private readonly IMessageStore _messageStore;
        private readonly ILogger<CachingStore> _logger;

        public CachingStore(IMessageStore messageStore, Container container, ILogger<CachingStore> logger)
        {
            _messageStore = messageStore;
            _container = container;
            _logger = logger;
        }

        public void AddPersistCollectionTask(
            Func<ICollection<PartitionState>, CancellationToken, Task> persistCollectionTask, Action clearTask)
        {
            _persistCollectionTasks.Add(persistCollectionTask);
            _clearTasks.Add(clearTask);
        }

        public void Clear()
        {
            foreach (var clearTask in _clearTasks)
            {
                clearTask.Invoke();
            }
        }

        public async Task PersistStoreAsync(CancellationToken cancellationToken)
        {
            var currentPartitionStates = _messageStore.GetCurrentPartitionStates();
            // currentPartitionStates.ForEach(x =>
            //     _logger.LogInformation(
            //         "Partition: {Partition} - Committed: {CommittedOffset} - Current: {CurrentOffset}", x.Partition,
            //         x.CommittedOffset, x.CurrentOffset));

            var unitOfWork = _container.GetInstance<IUnitOfWork>();
            unitOfWork.DisableCaching();

            using var transaction = await unitOfWork.BeginTransactionAsync(cancellationToken);
            try
            {
                // Persist all collection tasks
                foreach (var persistCollectionTask in _persistCollectionTasks)
                {
                    await persistCollectionTask.Invoke(currentPartitionStates, cancellationToken);
                }

                // Save currentPartitionStates to DB
                foreach (var state in currentPartitionStates)
                {
                    if (state.CommittedOffset == state.CurrentOffset) continue;

                    var repo = _container.GetRequiredService<IRepository<DbPartitionState, int>>();
                    var dbPartitionState =
                        await repo.FindOneAsync(x => x.Partition == state.Partition, cancellationToken);

                    if (dbPartitionState is null) continue;

                    dbPartitionState.CommittedOffset = state.CurrentOffset;
                    await repo.UpdateAsync(dbPartitionState, cancellationToken);
                    await _messageStore.UpdateCommittedOffsetAsync(state.Partition,
                        state.CurrentOffset,
                        cancellationToken);
                }

                await transaction.CommitAsync(cancellationToken);
            }
            catch (Exception e)
            {
                await transaction.RollbackAsync(cancellationToken);
                _logger.LogError("{Error}", e.Message);
            }
        }

        public async Task PersistCollectionAsync<TEntity, TId>(
            IList<CachingRecord<TEntity, TId>> insertedEntities,
            IList<CachingRecord<TEntity, TId>> modifiedEntities,
            IList<CachingRecord<TEntity, TId>> deletedEntities,
            CancellationToken cancellationToken = default) where TEntity : IEntity<TId>
        {
            try
            {
                var repo = _container.GetInstance<IRepository<TEntity, TId>>();
                repo.DisableCaching();

                await repo.AddRangeAsync(insertedEntities.Select(x => x.Entity), cancellationToken);
                await repo.UpdateRangeAsync(modifiedEntities.Select(x => x.Entity), cancellationToken);
                await repo.DeleteRangeAsync(deletedEntities.Select(x => x.Entity), cancellationToken);
            }
            catch (Exception e)
            {
                _logger.LogError("{Error}", e.Message);
            }
        }
    }
}