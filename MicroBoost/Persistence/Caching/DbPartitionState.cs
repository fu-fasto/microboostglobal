﻿using MicroBoost.Types;

namespace MicroBoost.Persistence.Caching
{
    public class DbPartitionState : EntityBase<int>
    {
        public override int Id { get; set; }
        public int Partition { get; set; }
        public long CommittedOffset { get; set; }
    }
}