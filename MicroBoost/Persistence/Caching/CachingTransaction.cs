﻿using System.Threading;
using System.Threading.Tasks;

namespace MicroBoost.Persistence.Caching
{
    public class CachingTransaction : ITransaction
    {
        private readonly CachingUnitOfWork _cachingUnitOfWork;

        public CachingTransaction(CachingUnitOfWork cachingUnitOfWork)
        {
            _cachingUnitOfWork = cachingUnitOfWork;
        }

        public void Dispose()
        {
        }

        public Task CommitAsync(CancellationToken cancellationToken = default)
        {
            _cachingUnitOfWork.CommitTransaction();
            return Task.CompletedTask;
        }

        public Task RollbackAsync(CancellationToken cancellationToken = default)
        {
            return Task.CompletedTask;
        }
    }
}