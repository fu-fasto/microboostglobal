﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;
using MicroBoost.Types;
using Microsoft.Extensions.Logging;

namespace MicroBoost.Persistence.Caching
{
    public class CachingCollection<TEntity, TId> where TEntity : class, IEntity<TId>
    {
        private static bool _singleTonInitialized;

        private static IDictionary<TId, CachingRecord<TEntity, TId>> _collection =
            new ConcurrentDictionary<TId, CachingRecord<TEntity, TId>>();

        private readonly IDictionary<TId, CachingRecord<TEntity, TId>> _transactionCollection =
            new ConcurrentDictionary<TId, CachingRecord<TEntity, TId>>();

        private readonly CachingStore _cachingStore;
        private readonly CachingUnitOfWork _unitOfWork;
        private readonly ILogger<CachingCollection<TEntity, TId>> _logger;

        public CachingCollection(CachingStore cachingStore, IUnitOfWork unitOfWork,
            ILogger<CachingCollection<TEntity, TId>> logger)
        {
            _cachingStore = cachingStore;
            _logger = logger;
            _unitOfWork = (CachingUnitOfWork) unitOfWork;
            _unitOfWork.AddCommitTransactionTask(CommitTransaction);

            if (!_singleTonInitialized)
            {
                _cachingStore.AddPersistCollectionTask(PersistAsync, Clear);
                _singleTonInitialized = true;
            }
        }

        public TEntity Get(TId id)
        {
            _collection.TryGetValue(id, out var record);
            return record?.Entity;
        }

        public void AddOrUpdate(TEntity entity, CommandPartition commandPartition, CachingRecordState state)
        {
            if (_unitOfWork.CachingTransaction is null)
            {
                AddOrUpdateToCollection(entity, commandPartition, state, _collection);
            }
            else
            {
                var oldEntity = Get(entity.Id);
                if (oldEntity is not null)
                {
                    AddOrUpdateToCollection(oldEntity, commandPartition, state, _transactionCollection);
                }

                AddOrUpdateToCollection(entity, commandPartition, state, _transactionCollection);
            }
        }

        public void Remove(TId id)
        {
            _collection.Remove(id);
        }

        public void Clear()
        {
            if (typeof(TEntity) == typeof(DbPartitionState))
            {
                return;
            }

            _collection = new ConcurrentDictionary<TId, CachingRecord<TEntity, TId>>();
        }

        private void CommitTransaction()
        {
            foreach (var cachingRecord in _transactionCollection.Values)
            {
                _collection[cachingRecord.Entity.Id] = cachingRecord;
            }
        }

        private void AddOrUpdateToCollection(TEntity entity, CommandPartition commandPartition,
            CachingRecordState state,
            IDictionary<TId, CachingRecord<TEntity, TId>> collection)
        {
            if (!collection.ContainsKey(entity.Id))
            {
                collection[entity.Id] = new()
                {
                    Entity = entity,
                    Partition = commandPartition.Partition,
                    Offset = commandPartition.Offset,
                    State = state
                };
            }
            else
            {
                collection[entity.Id].Entity = entity;
                collection[entity.Id].Partition = commandPartition.Partition;
                collection[entity.Id].Offset = commandPartition.Offset;
                collection[entity.Id].State =
                    collection[entity.Id].State == CachingRecordState.Inserted && state == CachingRecordState.Modified
                        ? CachingRecordState.Inserted
                        : state;
            }
        }

        private async Task PersistAsync(
            ICollection<PartitionState> currentPartitionStates,
            CancellationToken cancellationToken = default)
        {
            var insertedEntities = GetRecords(currentPartitionStates, CachingRecordState.Inserted);
            var modifiedEntities = GetRecords(currentPartitionStates, CachingRecordState.Modified);
            var deletedEntities = GetRecords(currentPartitionStates, CachingRecordState.Deleted);

            insertedEntities.ToList().ForEach(x => { _collection[x.Entity.Id].State = CachingRecordState.Normal; });
            modifiedEntities.ToList().ForEach(x => { _collection[x.Entity.Id].State = CachingRecordState.Normal; });
            deletedEntities.ToList().ForEach(x => { _collection[x.Entity.Id].State = CachingRecordState.Normal; });

            await _cachingStore.PersistCollectionAsync(insertedEntities, modifiedEntities,
                deletedEntities, cancellationToken);
        }

        private IList<CachingRecord<TEntity, TId>> GetRecords(IEnumerable<PartitionState> currentPartitionStates,
            CachingRecordState cachingRecordState)
        {
            return _collection.Values.Where(record =>
            {
                var partitionState = currentPartitionStates.FirstOrDefault(x => x.Partition == record.Partition);
                if (partitionState is null) return false;

                return record.State == cachingRecordState &&
                       (record.Offset == 0 ||
                        record.Offset > partitionState.CommittedOffset &&
                        record.Offset <= partitionState.CurrentOffset);
            }).ToList();
        }
    }
}