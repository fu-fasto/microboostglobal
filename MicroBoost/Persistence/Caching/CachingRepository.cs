﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Types;

namespace MicroBoost.Persistence.Caching
{
    public class CachingRepository<TEntity, T> : IRepository<TEntity, T> where TEntity : class, IEntity<T>
    {
        private readonly CachingCollection<TEntity, T> _cachingCollection;
        private readonly CommandPartition _commandPartition;
        private readonly IRepository<TEntity, T> _repository;
        private bool _isEnabled = true;

        public CachingRepository(CachingCollection<TEntity, T> cachingCollection, IRepository<TEntity, T> repository,
            CommandPartition commandPartition)
        {
            _cachingCollection = cachingCollection;
            _repository = repository;
            _commandPartition = commandPartition;
        }

        public Task<bool> IsExistsAsync(Expression<Func<TEntity, bool>> filter,
            CancellationToken cancellationToken = default)
        {
            return _repository.IsExistsAsync(filter, cancellationToken);
        }

        public Task<bool> IsExistsAsync(T id, CancellationToken cancellationToken = default)
        {
            return _repository.IsExistsAsync(id, cancellationToken);
        }

        public Task<TEntity> FindOneAsync(Expression<Func<TEntity, bool>> filter,
            CancellationToken cancellationToken = default)
        {
            return _repository.FindOneAsync(filter, cancellationToken);
        }

        public Task<TEntity> FindOneAsync(T id, CancellationToken cancellationToken = default)
        {
            return _repository.FindOneAsync(id, cancellationToken);
        }

        public Task<IEnumerable<TEntity>> FindAsync(IFindQuery<TEntity> findQuery = default(FindQuery<TEntity>),
            CancellationToken cancellationToken = default)
        {
            return _repository.FindAsync(findQuery, cancellationToken);
        }

        public Task<PagedResult<TEntity>> BrowseAsync(IFindQuery<TEntity> findQuery = default(FindQuery<TEntity>),
            CancellationToken cancellationToken = default)
        {
            return _repository.BrowseAsync(findQuery, cancellationToken);
        }

        public Task AddAsync(TEntity entity, CancellationToken cancellationToken)
        {
            _cachingCollection.AddOrUpdate(entity, _commandPartition, CachingRecordState.Inserted);

            return Task.CompletedTask;
        }

        public async Task AddRangeAsync(IEnumerable<TEntity> entities,
            CancellationToken cancellationToken)
        {
            if (_isEnabled)
                foreach (var entity in entities)
                    await AddAsync(entity, cancellationToken);

            await _repository.AddRangeAsync(entities, cancellationToken);
        }

        public async Task UpdateRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default)
        {
            if (_isEnabled)
                foreach (var entity in entities)
                    await UpdateAsync(entity, cancellationToken);

            await _repository.UpdateRangeAsync(entities, cancellationToken);
        }

        public async Task DeleteOneAsync(T id, CancellationToken cancellationToken)
        {
            if (_isEnabled)
            {
                var entity = await FindOneAsync(id, cancellationToken);
                if (entity is not null)
                    _cachingCollection.AddOrUpdate(entity, _commandPartition, CachingRecordState.Deleted);
            }

            await _repository.DeleteOneAsync(id, cancellationToken);
        }

        public async Task DeleteRangeAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default)
        {
            if (_isEnabled)
                foreach (var entity in entities)
                    await AddAsync(entity, cancellationToken);

            await _repository.DeleteRangeAsync(entities, cancellationToken);
        }

        public Task UpdateAsync(TEntity entity, CancellationToken cancellationToken)
        {
            if (_isEnabled)
            {
                _cachingCollection.AddOrUpdate(entity, _commandPartition, CachingRecordState.Modified);

                return Task.CompletedTask;
            }

            return _repository.UpdateAsync(entity, cancellationToken);
        }

        void IRepository<TEntity, T>.DisableCaching()
        {
            _isEnabled = false;
        }
    }
}