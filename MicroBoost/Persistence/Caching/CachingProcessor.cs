﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace MicroBoost.Persistence.Caching
{
    public class CachingProcessor
    {
        private readonly Container _container;
        private readonly ILogger<CachingProcessor> _logger;
        private readonly PersistenceOptions _options;
        private bool _isActivated;

        public CachingProcessor(Container container, ILogger<CachingProcessor> logger, PersistenceOptions options)
        {
            _container = container;
            _logger = logger;
            _options = options;
        }

        public Task Active(CancellationToken cancellationToken)
        {
            if (!_isActivated)
            {
                _isActivated = true;
                Task.Delay(TimeSpan.FromMilliseconds(_options.CachingDelayTime), cancellationToken)
                    .ContinueWith(async _ =>
                    {
                        await SaveToDatabase(cancellationToken);
                        _isActivated = false;
                        
                        await Task.Delay(TimeSpan.FromSeconds(1), cancellationToken)
                            .ContinueWith(async _ => { await SaveToDatabase(cancellationToken); }, cancellationToken);
                    }, cancellationToken);
            }

            return Task.CompletedTask;
        }

        private async Task SaveToDatabase(CancellationToken cancellationToken)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            await using (var scope = AsyncScopedLifestyle.BeginScope(_container))
            {
                var cachingStore = scope.GetInstance<CachingStore>();
                await cachingStore.PersistStoreAsync(cancellationToken);
            }

            stopwatch.Stop();
            _logger.LogInformation("Global write caching time taken: {Time}",
                stopwatch.Elapsed.ToString(@"m\:ss\.fff"));

            _isActivated = false;
        }
    }
}