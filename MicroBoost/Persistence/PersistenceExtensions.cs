﻿using System;
using MicroBoost.Jaeger;
using MicroBoost.Persistence.Caching;
using MicroBoost.Persistence.EfCore;
using MicroBoost.Persistence.Mongo;
using MicroBoost.Persistence.OutBox;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace MicroBoost.Persistence
{
    public static class PersistenceExtensions
    {
        private const string SectionName = "Persistence";

        public static IMicroBoostBuilder AddSqlPersistence<TContext>
            (this IMicroBoostBuilder builder)
            where TContext : DbContext
        {
            var options = builder.GetOptions<PersistenceOptions>(SectionName);
            EnableCore(builder, options);

            switch (options.DatabaseType)
            {
                case "mysql":
                case "sqlserver":
                case "mssql":
                    builder.AddEfCore<TContext>(options);
                    break;

                default:
                    throw new Exception($"Database type '{options.DatabaseType}' is not supported");
            }

            return builder;
        }

        public static IMicroBoostBuilder AddNoSqlPersistence(this IMicroBoostBuilder builder)
        {
            var options = builder.GetOptions<PersistenceOptions>(SectionName);
            EnableCore(builder, options);

            switch (options.DatabaseType)
            {
                case "mongo":
                case "mongodb":
                    builder.AddMongo(options);
                    break;

                default:
                    throw new Exception($"Database type '{options.DatabaseType}' is not supported");
            }

            return builder;
        }

        private static void EnableCore(IMicroBoostBuilder builder, PersistenceOptions options)
        {
            if (options.Tracing)
            {
                JaegerExtensions.AddRepositoryDecorator();
            }
            
            if (options.Caching)
            {
                builder.Container.RegisterSingleton<CachingStore>();
                builder.Container.Register(typeof(CachingCollection<,>), typeof(CachingCollection<,>));

                builder.Container.RegisterDecorator(typeof(IRepository<,>), typeof(CachingRepository<,>));
                builder.Container.RegisterDecorator<IUnitOfWork, CachingUnitOfWork>();

                builder.Container.RegisterSingleton<CachingProcessor>();
            }

            if (options.OutBox)
            {
                builder.Services.AddHostedService<OutBoxProcessor>();
            }
            
            builder.Services.AddHostedService<PersistenceSeeding>();
        }
    }
}