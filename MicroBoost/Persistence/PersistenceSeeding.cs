﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Helpers;
using MicroBoost.Types;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace MicroBoost.Persistence
{
    internal class PersistenceSeeding : IHostedService
    {
        private readonly Container _container;
        private readonly Assembly _callingAssembly;
        private readonly ILogger<PersistenceSeeding> _logger;

        public PersistenceSeeding(IMicroBoostBuilder builder, ILogger<PersistenceSeeding> logger)
        {
            _logger = logger;
            _container = builder.Container;
            _callingAssembly = builder.CallingAssembly;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Task.Run(Seeding, cancellationToken);
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        private String GetRawData()
        {
            try
            {
                var webHostEnvironment = _container.GetInstance<IWebHostEnvironment>();
                return System.IO.File.ReadAllText(webHostEnvironment.ContentRootPath + "/databaseSeeding.json");
            }
            catch (Exception)
            {
                return String.Empty;
            }
        }

        private async Task Seeding()
        {
            string rawData = GetRawData();
            if (String.IsNullOrWhiteSpace(rawData)) return;

            try
            {
                var entityTypes = _callingAssembly.GetTypesHaveGenericInterface(typeof(IEntity<>));

                var entityNames = entityTypes.Select(entityType => entityType.Name).ToList();
                var entityListTypes = entityTypes.Select(entityType => typeof(List<>).MakeGenericType(entityType))
                    .ToList();

                var seedingType =
                    new CustomClassBuilder("Dynamic").CreateObject(entityNames.ToArray(), entityListTypes.ToArray())
                        .GetType();
                dynamic seedingData = JsonConvert.DeserializeObject(rawData, seedingType);
                var seedingOrder = JsonConvert.DeserializeObject<Dictionary<string, List<Object>>>(rawData);

                await using (var scope = AsyncScopedLifestyle.BeginScope(_container))
                {
                    var unitOfWork = scope.GetInstance<IUnitOfWork>();
                    unitOfWork.DisableCaching();
                    // using var transaction = await unitOfWork.BeginTransactionAsync();

                    try
                    {
                        foreach (var entity in seedingOrder.Keys)
                        {
                            var entityType = entityTypes.FirstOrDefault(x => x.Name.ToLower().Equals(entity.ToLower()));
                            if (entityType is null) continue;

                            var idType = entityType.GetProperty("Id")!.GetValue(entityType.CreateInstance())!.GetType();

                            var entities = seedingType.GetProperty(entityType.Name)!.GetValue(seedingData);

                            var method = (Task) GetType().GetMethod("SeedingEntity")?
                                .MakeGenericMethod(entityType, idType)
                                .Invoke(this, new object?[] {unitOfWork, entities});
                            if (method != null) await method;
                        }

                        // await transaction.CommitAsync();
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("{Error}", e.Message);
                        // await transaction.RollbackAsync();
                    }
                }
                
                _logger.LogInformation("Database have seeded successfully");
            }
            catch (Exception e)
            {
                _logger.LogError("{Error}", e.Message);
            }
        }

        public async Task SeedingEntity<TEntity, TId>(IUnitOfWork unitOfWork, List<TEntity> entities)
            where TEntity : IEntity<TId>
        {
            if (entities is null || !entities.Any()) return;
            var repository = unitOfWork.GetRepository<TEntity, TId>();
            if (await repository.IsExistsAsync(x => true)) return;

            await repository.AddRangeAsync(entities);
        }
    }
}