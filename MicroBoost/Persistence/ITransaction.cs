﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace MicroBoost.Persistence
{
    public interface ITransaction : IDisposable
    {
        Task CommitAsync(CancellationToken cancellationToken = default);
        Task RollbackAsync(CancellationToken cancellationToken = default);
    }
}
