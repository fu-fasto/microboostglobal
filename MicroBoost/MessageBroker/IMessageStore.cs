﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;

namespace MicroBoost.MessageBroker
{
    public interface IMessageStore
    {
        List<PartitionState> GetCurrentPartitionStates();
        
        Task ResetPartitionStatesAsync(IConsumer<string, string> consumer, IList<TopicPartition> topicPartitions,
            CancellationToken cancellationToken = default);

        Task UpdateCommittedOffsetAsync(int partition, long committedOffset,
            CancellationToken cancellationToken = default);

        Task UpdateCurrentOffsetAsync(int partition, long currentOffset, CancellationToken cancellationToken = default);
    }
}