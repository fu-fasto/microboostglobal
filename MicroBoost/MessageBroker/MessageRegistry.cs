﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace MicroBoost.MessageBroker
{
    public static class MessageRegistry
    {
        private static readonly IDictionary<string, Type> MessageTypes =
            new Dictionary<string, Type>();

        private static readonly IDictionary<Type, Convention> Conventions =
            new Dictionary<Type, Convention>();

        public static Convention GetConvention(this IMessage message)
        {
            return Conventions[message.GetType()];
        }

        public static string GetMessageKey(this IMessage message)
        {
            return message.GetType().GetProperties()
                .FirstOrDefault(p => p.GetCustomAttribute<MessageKeyAttribute>() is { })
                ?.GetValue(message)?.ToString();
        }

        public static Type GetMessageType(string raw)
        {
            return MessageTypes[raw];
        }

        public static List<string> GetAllTopics()
        {
            return Conventions.Values.Where(c => c.Scope != MessageScope.Internal).Select(c => c.Topic).ToList();
        }

        public static void AddEvent(Type type)
        {
            // Push string-type 
            MessageTypes.Add(type.Name, type);

            // Push type-convention
            var messageAttribute = type.GetCustomAttribute<MessageAttribute>();
            if (messageAttribute != null)
                Conventions.Add(type, new()
                {
                    Topic = messageAttribute.Topic ?? type.Name,
                    Scope = messageAttribute.Scope
                });
        }
    }
}