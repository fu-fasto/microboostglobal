﻿using System.Threading;
using System.Threading.Tasks;

namespace MicroBoost.MessageBroker
{
    public interface IMessageSubscriber
    {
        internal Task SubscribeEventAsync(string topic, CancellationToken cancellationToken = default);
        internal Task SubscribeCommandAsync(string topic, CancellationToken cancellationToken = default);
    }
}
