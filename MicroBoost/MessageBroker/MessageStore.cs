﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using MicroBoost.Cqrs;
using MicroBoost.Persistence;
using MicroBoost.Persistence.Caching;
using Microsoft.Extensions.DependencyInjection;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace MicroBoost.MessageBroker
{
    public class MessageStore : IMessageStore
    {
        private readonly CqrsOptions _cqrsOptions;
        private readonly Container _container;

        private readonly IDictionary<int, PartitionState> _partitionStates =
            new ConcurrentDictionary<int, PartitionState>();
        
        public MessageStore(CqrsOptions cqrsOptions, Container container)
        {
            _cqrsOptions = cqrsOptions;
            _container = container;
        }

        public List<PartitionState> GetCurrentPartitionStates()
        {
            return _partitionStates.Values.Select(x => new PartitionState
            {
                Partition = x.Partition,
                CommittedOffset = x.CommittedOffset,
                CurrentOffset = x.CurrentOffset
            }).ToList();
        }

        public async Task ResetPartitionStatesAsync(IConsumer<string, string> consumer,
            IList<TopicPartition> topicPartitions, CancellationToken cancellationToken = default)
        {
            var topicPartitionOffsets = consumer.Committed(topicPartitions, TimeSpan.FromSeconds(3));
            await using var scope = AsyncScopedLifestyle.BeginScope(_container);
            var repo = scope.GetRequiredService<IRepository<DbPartitionState, int>>();

            var dbPartitionStates = topicPartitionOffsets.Select(
                topicPartitionOffset =>
                {
                    // Disable cache for DbPartitionState
                    repo.DisableCaching();
                    
                    var dbPartitionState = repo.FindOneAsync(x => x.Partition == topicPartitionOffset.Partition.Value,
                        cancellationToken).Result;
                    if (dbPartitionState is not null) return dbPartitionState;
                    
                    dbPartitionState = new DbPartitionState
                    {
                        Id = topicPartitionOffset.Partition.Value + 1,
                        Partition = topicPartitionOffset.Partition.Value,
                        CommittedOffset = 0
                    };
                    repo.AddAsync(dbPartitionState, cancellationToken).Wait(cancellationToken);
                    return dbPartitionState;
                }).ToList();

            await ResetAllAsync(dbPartitionStates, cancellationToken);
            consumer.Commit(dbPartitionStates.Select(dbPartitionState => new TopicPartitionOffset(
                _cqrsOptions.CommandQueueName, new Partition(dbPartitionState.Partition),
                new Offset(dbPartitionState.CommittedOffset)
            )));
        }

        public Task UpdateCommittedOffsetAsync(int partition, long committedOffset, CancellationToken cancellationToken)
        {
            if (_partitionStates.TryGetValue(partition, out var globalPartitionState))
                globalPartitionState.CommittedOffset = committedOffset;

            return Task.CompletedTask;
        }

        public Task UpdateCurrentOffsetAsync(int partition, long currentOffset, CancellationToken cancellationToken)
        {
            if (_partitionStates.TryGetValue(partition, out var globalPartitionState))
                globalPartitionState.CurrentOffset = currentOffset;

            return Task.CompletedTask;
        }

        private Task ResetAllAsync(ICollection<DbPartitionState> dbPartitionStates, CancellationToken cancellationToken)
        {
            _container.GetRequiredService<CachingStore>().Clear();

            // Clean old
            foreach (var partition in _partitionStates.Keys)
                if (_partitionStates.Values.FirstOrDefault(x => x.Partition == partition) is not null)
                    _partitionStates.Remove(partition);

            // Add or Update
            foreach (var dbPartitionState in dbPartitionStates)
                if (!_partitionStates.ContainsKey(dbPartitionState.Partition))
                    _partitionStates.Add(dbPartitionState.Partition, new PartitionState
                    {
                        Partition = dbPartitionState.Partition,
                        CommittedOffset = dbPartitionState.CommittedOffset,
                        CurrentOffset = dbPartitionState.CommittedOffset
                    });

            return Task.CompletedTask;
        }
    }
}