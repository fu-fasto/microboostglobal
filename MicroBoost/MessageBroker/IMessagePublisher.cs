﻿using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Persistence.OutBox;

namespace MicroBoost.MessageBroker
{
    public interface IMessagePublisher
    {
        Task PublishAsync(IMessage message, CancellationToken cancellationToken = default);
        
        Task PublishAsync(IEnumerable<OutBoxMessage> outBoxMessages, CancellationToken cancellationToken = default);

        Task PublishOutBoxAsync(IMessage message, CancellationToken cancellationToken = default);
    }
}