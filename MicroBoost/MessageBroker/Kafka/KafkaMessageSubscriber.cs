﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using Confluent.Kafka.Admin;
using MediatR;
using MicroBoost.Cqrs;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Events;
using MicroBoost.Helpers;
using MicroBoost.Persistence;
using MicroBoost.Persistence.Caching;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace MicroBoost.MessageBroker.Kafka
{
    public class KafkaMessageSubscriber : IMessageSubscriber
    {
        private const string MessageBrokerName = "Kafka";
        private readonly Container _container;
        private readonly CqrsOptions _cqrsOptions;
        private readonly KafkaOptions _kafkaOptions;
        private readonly ILogger _logger;
        private readonly IMessageStore _messageStore;
        private readonly IMicroBoostBuilder _microBoostBuilder;
        private readonly PersistenceOptions _persistenceOptions;

        public KafkaMessageSubscriber(ILogger<KafkaMessageSubscriber> logger, KafkaOptions kafkaOptions,
            CqrsOptions cqrsOptions,
            IMessageStore messageStore, PersistenceOptions persistenceOptions, Container container,
            IMicroBoostBuilder microBoostBuilder)
        {
            _kafkaOptions = kafkaOptions;
            _cqrsOptions = cqrsOptions;
            _messageStore = messageStore;
            _persistenceOptions = persistenceOptions;
            _container = container;
            _microBoostBuilder = microBoostBuilder;
            _logger = logger;

            // Register declared events
            InitSubscriber();
        }

        Task IMessageSubscriber.SubscribeEventAsync(string topic, CancellationToken cancellationToken)
        {
            _ = Task.Run(async () =>
            {
                await InitTopic(topic);

                using var consumer = new ConsumerBuilder<string, string>(_kafkaOptions.Consumer)
                    .SetPartitionsAssignedHandler((_, _) =>
                    {
                        _logger.LogInformation("Partition assigned from {Topic}", topic);
                    })
                    .Build();
                consumer.Subscribe(topic);

                while (true)
                {
                    try
                    {
                        var raw = consumer.Consume();
                        var headers = GetMessageHeaders(raw);
                        var message = JsonConvert.DeserializeObject(raw.Message.Value,
                            MessageRegistry.GetMessageType(headers["Type"]));

                        // Create event scope
                        await using var scope = AsyncScopedLifestyle.BeginScope(_container);
                        MicroBoostHelpers.GetTracingContext()?.TryBuildRootSpanContext(headers["SpanContext"]);
                        var mediator = scope.GetRequiredService<IMediator>();
                        await mediator.Publish(message, cancellationToken);
                        
                        // Active db write caching
                        if (_cqrsOptions.UseMessageQueueCommand && _persistenceOptions.Caching)
                        {
                            await scope.GetRequiredService<CachingProcessor>().Active(cancellationToken);
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("{Message}", e.Message);
                    }

                    consumer.Commit();
                }
            }, cancellationToken);

            return Task.CompletedTask;
        }

        Task IMessageSubscriber.SubscribeCommandAsync(string topic, CancellationToken cancellationToken)
        {
            _ = Task.Run(async () =>
            {
                await InitTopic(topic);

                using var consumer = new ConsumerBuilder<string, string>(_kafkaOptions.Consumer)
                    .SetPartitionsAssignedHandler(async (c, topicPartitions) =>
                    {
                        _logger.LogInformation("Partition assigned from {Topic}", topic);
                        if (!_persistenceOptions.Caching) return;

                        try
                        {
                            await _messageStore.ResetPartitionStatesAsync(c, topicPartitions, cancellationToken);
                        }
                        catch (Exception e)
                        {
                            _logger.LogError("{Error}", e.Message);
                        }
                    })
                    .Build();
                consumer.Subscribe(topic);

                while (true)
                {
                    try
                    {
                        var raw = consumer.Consume();
                        var headers = GetMessageHeaders(raw);
                        var message = JsonConvert.DeserializeObject(raw.Message.Value,
                            MessageRegistry.GetMessageType(headers["Type"])) as ICommand;

                        // Create command scope
                        await using var scope = AsyncScopedLifestyle.BeginScope(_container);
                        
                        // Update CommandPartition
                        if (_cqrsOptions.UseMessageQueueCommand)
                        {
                            var topicPartitionOffset = raw.TopicPartitionOffset;
                            var commandPartition = scope.GetRequiredService<CommandPartition>();
                            commandPartition.Partition = topicPartitionOffset.Partition.Value;
                            commandPartition.Offset = topicPartitionOffset.Offset.Value + 1;
                        }
                        
                        // Process main business
                        MicroBoostHelpers.GetTracingContext()?.TryBuildRootSpanContext(headers["SpanContext"]);
                        var mediator = scope.GetRequiredService<IMediator>();
                        await mediator.Publish(message, cancellationToken);
                        
                        // Commit offset
                        if (_cqrsOptions.UseMessageQueueCommand)
                        {
                            var commandPartition = scope.GetRequiredService<CommandPartition>();

                            await _container.GetRequiredService<IMessageStore>()
                                .UpdateCurrentOffsetAsync(commandPartition.Partition,
                                    commandPartition.Offset, cancellationToken);

                            await scope.GetRequiredService<CachingProcessor>().Active(cancellationToken);
                        }
                    }
                    catch (Exception e)
                    {
                        _logger.LogError("{Message}", e.Message);
                    }

                    // Commit if not persistence caching
                    if (!_persistenceOptions.Caching) consumer.Commit();
                }
            }, cancellationToken);

            return Task.CompletedTask;
        }

        private Dictionary<string, string> GetMessageHeaders(ConsumeResult<string, string> message)
        {
            return message.Message.Headers
                .Aggregate(new Dictionary<string, string>(), (result, header) =>
                {
                    result.Add(header.Key, Encoding.UTF8.GetString(header.GetValueBytes()));
                    return result;
                });
        }

        private async Task InitTopic(string topic)
        {
            try
            {
                using var client = new AdminClientBuilder(_kafkaOptions.Producer).Build();
                await client.CreateTopicsAsync(new[]
                {
                    new TopicSpecification
                    {
                        Name = topic,
                        NumPartitions = _kafkaOptions.NumPartitions
                    }
                });
            }
            catch (Exception)
            {
                // ignored
            }
        }

        private void InitSubscriber()
        {
            // Register declared events
            _microBoostBuilder.CallingAssembly.GetTypes()
                .Where(type =>
                    type.GetInterfaces().Contains(typeof(IEvent)) &&
                    type.GetCustomAttribute<MessageAttribute>() is not null)
                .ToList().ForEach(MessageRegistry.AddEvent);

            // Subscribe all events
            MessageRegistry.GetAllTopics().ForEach(topic => ((IMessageSubscriber) this).SubscribeEventAsync(topic));

            // Subscribe command queue
            if (_cqrsOptions.UseMessageQueueCommand)
            {
                _microBoostBuilder.CallingAssembly.GetTypes()
                    .Where(type =>
                        type.GetInterfaces().Contains(typeof(ICommand)) &&
                        type.GetCustomAttribute<MessageAttribute>() is not null)
                    .ToList().ForEach(MessageRegistry.AddEvent);
                ((IMessageSubscriber) this).SubscribeCommandAsync(_cqrsOptions.CommandQueueName);
            }
        }
    }
}