﻿using Confluent.Kafka;

namespace MicroBoost.MessageBroker.Kafka
{
    public class KafkaOptions
    {
        public ProducerConfig Producer { get; set; }
        public ConsumerConfig Consumer { get; set; }

        public int NumPartitions { get; set; } = 4;
        
        public KafkaOptions(MessageBrokerOptions options)
        {
            Producer = new()
            {
                BootstrapServers = options.ConnectionString,
                Partitioner = Partitioner.ConsistentRandom,
                Acks = Acks.All,
                // MaxInFlight = 5,
                LingerMs = 0
            };
            Consumer = new()
            {
                BootstrapServers = options.ConnectionString,
                GroupId = options.GroupId,
                EnableAutoCommit = false,
                AutoOffsetReset = AutoOffsetReset.Earliest,
                AllowAutoCreateTopics = true
            };
        }
    }
}
