﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Confluent.Kafka;
using MicroBoost.Cqrs;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.Jaeger;
using MicroBoost.Persistence;
using MicroBoost.Persistence.OutBox;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using SimpleInjector;

namespace MicroBoost.MessageBroker.Kafka
{
    public class KafkaMessagePublisher : IMessagePublisher
    {
        private readonly CqrsOptions _cqrsOptions;
        private readonly MessageBrokerOptions _messageBrokerOptions;
        private readonly ILogger _logger;
        private readonly IProducer<string, string> _producer;
        private readonly Container _container;

        public KafkaMessagePublisher(
            IProducer<string, string> producer,
            ILogger<KafkaMessagePublisher> logger,
            CqrsOptions cqrsOptions,
            Container container, MessageBrokerOptions messageBrokerOptions)
        {
            _producer = producer;
            _logger = logger;
            _cqrsOptions = cqrsOptions;
            _container = container;
            _messageBrokerOptions = messageBrokerOptions;
        }


        public Task PublishAsync(IMessage message, CancellationToken cancellationToken)
        {
            var topic = message is ICommand ? _cqrsOptions.CommandQueueName : message.GetConvention().Topic;
            var value = JsonConvert.SerializeObject(message);
            var type = message.GetType().Name;
            var key = message.GetMessageKey();

            return PublishRawAsync(topic, key, type, value, cancellationToken);
        }

        public Task PublishAsync(IEnumerable<OutBoxMessage> outBoxMessages,
            CancellationToken cancellationToken = default)
        {
            foreach (var outBoxMessage in outBoxMessages)
            {
                MicroBoostHelpers.GetTracingContext()?.TryBuildRootSpanContext(outBoxMessage.SpanContext);
                var tracer = JaegerExtensions.InitTracer(_messageBrokerOptions.Type);
                var messageQueueScope = tracer?.StartSpanScope("Publish from outbox to queue");

                var kafkaMessage = BuildKafkaMessage(outBoxMessage.Key, outBoxMessage.Type,
                    outBoxMessage.Value, outBoxMessage.SpanContext);
                _producer.Produce(outBoxMessage.Topic, kafkaMessage);

                messageQueueScope?.Dispose();
                tracer?.Dispose();
            }

            _producer.Flush(cancellationToken);
            return Task.CompletedTask;
        }

        public async Task PublishOutBoxAsync(IMessage message, CancellationToken cancellationToken)
        {
            var repository = _container.GetInstance<IRepository<OutBoxMessage, Guid>>();
            var commandPartition = _container.GetInstance<CommandPartition>();
            var currentSpan = MicroBoostHelpers.GetCurrentSpan();

            var topic = message is ICommand ? _cqrsOptions.CommandQueueName : message.GetConvention().Topic;
            var outBoxMessage = new OutBoxMessage
            {
                Key = message.GetMessageKey(),
                Value = JsonConvert.SerializeObject(message),
                Topic = topic,
                Type = message.GetType().Name,
                SpanContext = currentSpan?.Context?.ToString(),
                Partition = commandPartition.Partition,
                Offset = commandPartition.Offset
            };

            await repository.AddAsync(outBoxMessage, cancellationToken);
        }

        private Task PublishRawAsync(string topic, string key, string type, string value,
            CancellationToken cancellationToken)
        {
            var currentSpan = MicroBoostHelpers.GetCurrentSpan();
            return _producer.ProduceAsync(topic, BuildKafkaMessage(key, type, value, currentSpan?.Context?.ToString()),
                cancellationToken);
        }

        private Message<string, string> BuildKafkaMessage(string key, string type, string value,
            string spanContext = null)
        {
            return new()
            {
                Key = key,
                Value = value,
                Headers = new Headers
                {
                    {"Type", Encoding.UTF8.GetBytes(type)},
                    {"SpanContext", Encoding.UTF8.GetBytes(spanContext ?? string.Empty)}
                }
            };
        }
    }
}