﻿using Confluent.Kafka;
using MicroBoost.Persistence;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace MicroBoost.MessageBroker.Kafka
{
    public static class KafkaExtensions
    {
        private const string SectionName = "MessageBroker";

        internal static IMicroBoostBuilder AddKafka(this IMicroBoostBuilder builder)
        {
            var options = builder.GetOptions<MessageBrokerOptions>(SectionName);
            builder.GetOptions<PersistenceOptions>("PersistenceOptions");

            if (options.EnabledPublisher)
            {
                builder.Services.AddSingleton(sp =>
                {
                    var kafkaOptions = sp.GetRequiredService<KafkaOptions>();
                    return new ProducerBuilder<string, string>(kafkaOptions.Producer).SetErrorHandler((_, err) =>
                    {
                        sp.GetService<ILoggerFactory>().CreateLogger<KafkaMessageSubscriber>().LogError(err.Reason);
                    }).Build();
                });
                builder.Container.RegisterSingleton<IMessagePublisher, KafkaMessagePublisher>();
            }

            if (options.EnabledSubscriber)
            {
                builder.Container.RegisterSingleton<IMessageSubscriber, KafkaMessageSubscriber>();
                builder.Container.RegisterSingleton<IMessageStore, MessageStore>();
            }

            return builder;
        }
    }
}