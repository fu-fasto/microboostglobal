﻿using System;

namespace MicroBoost.MessageBroker
{
    [AttributeUsage(AttributeTargets.Property)]
    public class MessageKeyAttribute : Attribute
    {
    }
}