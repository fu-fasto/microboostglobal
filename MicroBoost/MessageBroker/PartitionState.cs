﻿namespace MicroBoost.MessageBroker
{
    public class PartitionState
    {
        public int Partition { get; set; }
        public long CommittedOffset { get; set; }
        public long CurrentOffset { get; set; }
    }
}