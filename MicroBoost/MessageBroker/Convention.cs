﻿namespace MicroBoost.MessageBroker
{
    public class Convention
    {
        public string Topic { get; set; }
        public MessageScope Scope { get; set; }
    }
}