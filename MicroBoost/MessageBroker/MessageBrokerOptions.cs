﻿namespace MicroBoost.MessageBroker
{
    public class MessageBrokerOptions
    {
        public string GroupId { get; set; }
        public string Type { get; set; }
        public string ConnectionString { get; set; }
        public bool EnabledSubscriber { get; set; }
        public bool EnabledPublisher { get; set; }
    }
}