﻿using System;
using MicroBoost.Jaeger;
using MicroBoost.MessageBroker.Kafka;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace MicroBoost.MessageBroker
{
    public static class MessageBrokerExtensions
    {
        private const string SectionName = "MessageBroker";

        public static IMicroBoostBuilder AddMessageBroker(this IMicroBoostBuilder builder)
        {
            var options = builder.GetOptions<MessageBrokerOptions>(SectionName);
            switch (options.Type.ToLower())
            {
                case "kafka":
                    options.Type = "Kafka";
                    builder.AddKafka();
                    builder.Services.TryAddSingleton(new KafkaOptions(options));
                    break;

                default:
                    throw new Exception($"MessageBroker type '{options.Type}' is not supported");
            }

            // Add tracing decorator
            JaegerExtensions.AddMessageBrokerDecorator();
            
            return builder;
        }

        public static IApplicationBuilder UseMessageBroker(this IApplicationBuilder app)
        {
            return app;
        }
    }
}