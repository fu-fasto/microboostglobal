﻿using System;

namespace MicroBoost.MessageBroker
{
    [AttributeUsage(AttributeTargets.Class)]
    public class MessageAttribute : Attribute
    {
        public string Topic { get; }
        public MessageScope Scope { get; }

        public MessageAttribute()
        {
            Scope = MessageScope.Internal;
        }
        
        public MessageAttribute(string topic = null, MessageScope scope = MessageScope.Internal)
        {
            Topic = topic;
            Scope = scope;
        }
        
        public MessageAttribute(MessageScope scope = MessageScope.Internal, string topic = null)
        {
            Topic = topic;
            Scope = scope;
        }
    }

    public enum MessageScope
    {
        Internal, External, Both
    }
}