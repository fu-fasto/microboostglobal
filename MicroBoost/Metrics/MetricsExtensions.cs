using Microsoft.AspNetCore.Builder;
using Prometheus;

namespace MicroBoost.Metrics
{
    public static class MetricsExtensions
    {
        public static IApplicationBuilder UseMetrics(this IApplicationBuilder app)
        {
            app.UseMetricServer();
            app.UseHttpMetrics();
            
            return app;
        }
    }
}