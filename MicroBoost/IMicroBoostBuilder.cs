﻿using System;
using System.Reflection;
using Microsoft.Extensions.DependencyInjection;
using SimpleInjector;

namespace MicroBoost
{
    public interface IMicroBoostBuilder
    {
        Assembly CallingAssembly { get; }
        Container Container { get; }
        IServiceCollection Services { get; }
        IServiceProvider Build();
    }
}