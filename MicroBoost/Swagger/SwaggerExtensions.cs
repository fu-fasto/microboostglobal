﻿using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;

namespace MicroBoost.Swagger
{
    public static class SwaggerExtensions
    {
        private const string SectionName = "swagger";
        
        public static IMicroBoostBuilder AddSwaggerDocs(this IMicroBoostBuilder builder)
        {
            var options = builder.GetOptions<SwaggerOptions>(SectionName);
            var filePath = Path.Combine(AppContext.BaseDirectory, $"{options.Title}.xml");

            builder.Services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc(options.Version, options);
                c.CustomSchemaIds(x => x.FullName);
                //c.IncludeXmlComments(filePath);
                if (options.IncludeSecurity)
                {
                    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                    {
                        Name = "Authorization",
                        In = ParameterLocation.Header,
                        Type = SecuritySchemeType.Http,
                        Scheme = "Bearer",
                        BearerFormat = "JWT",
                        Description = "Input your Bearer token in this format - Bearer {your token here} to access this API",
                    });

                    c.AddSecurityRequirement(new OpenApiSecurityRequirement
                    {
                        {
                            new OpenApiSecurityScheme
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "Bearer",
                                },
                                Scheme = "Bearer",
                                Name = "Authorization",
                                In = ParameterLocation.Header,
                            }, new List<string>()
                        },
                    });
                }
            });
            

            return builder;
        }

        public static IApplicationBuilder UseSwaggerDocs(this IApplicationBuilder app)
        {
            var options = app.ApplicationServices.GetService<SwaggerOptions>();

            app.UseSwagger();

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint($"swagger/{options.Version}/swagger.json", options.Title);
                c.RoutePrefix = options.RoutePrefix;
            });

            return app;
        }
    }
}
