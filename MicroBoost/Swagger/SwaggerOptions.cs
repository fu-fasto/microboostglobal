﻿using System;
using Microsoft.OpenApi.Models;

namespace MicroBoost.Swagger
{
    public class SwaggerOptions : OpenApiInfo
    {
        public bool Enabled { get; set; } = true;
        public new string Title { get; set; } = AppDomain.CurrentDomain.FriendlyName.Trim().Trim('_');
        public new string Version { get; set; } = "v1";
        public string RoutePrefix { get; set; } = string.Empty;
        public bool IncludeSecurity { get; set; }
    }
}
