﻿using System;
using System.Reflection;
using MicroBoost.Helpers;
using Microsoft.Extensions.DependencyInjection;
using SimpleInjector;
using SimpleInjector.Lifestyles;

namespace MicroBoost
{
    public class MicroBoostBuilder : IMicroBoostBuilder
    {
        public Assembly CallingAssembly { get; }
        public Container Container { get; }
        public IServiceCollection Services { get; }

        private MicroBoostBuilder(IServiceCollection services, Assembly callingAssembly)
        {
            Services = services;
            Container = new Container();
            CallingAssembly = callingAssembly;

            Container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
            Container.Options.DefaultLifestyle = Lifestyle.Scoped;
            MicroBoostHelpers.Container = Container;
        }


        public IServiceProvider Build()
        {
            Services.AddSimpleInjector(Container, options =>
            {
                options.AddAspNetCore()
                    .AddControllerActivation()
                    .AddViewComponentActivation()
                    .AddPageModelActivation()
                    .AddTagHelperActivation();
            });
            var serviceProvider = Services.BuildServiceProvider(true);
            serviceProvider.UseSimpleInjector(Container);
            
            Container.Verify();
            return serviceProvider;
        }

        public static MicroBoostBuilder Create(IServiceCollection services, Assembly callingAssembly)
        {
            return new(services, callingAssembly);
        }
    }
}