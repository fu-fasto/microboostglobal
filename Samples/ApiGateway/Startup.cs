using MicroBoost;
using MicroBoost.Auth;
using MicroBoost.Swagger;
using MicroBoost.WebAPI;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Ocelot.Cache.CacheManager;
using Ocelot.DependencyInjection;
using Ocelot.Middleware;
using Ocelot.Provider.Consul;

namespace ApiGateway
{
    public class Startup
    {
        private readonly IConfiguration Configuration;

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMicroBoostBuilder()
                .AddWebApi()
                .AddAuth()
                .AddSwaggerDocs()
                .Build();

            services
                .AddSwaggerForOcelot(Configuration)
                .AddOcelot(Configuration)
                .AddCacheManager(x => { x.WithDictionaryHandle(); })
                .AddConsul();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseWebApi();
            app.UseSwaggerForOcelotUI(o => { o.PathToSwaggerGenerator = "/swagger/docs"; });
            app.UseSwaggerDocs();
            app.UseOcelot().Wait();
        }
    }
}