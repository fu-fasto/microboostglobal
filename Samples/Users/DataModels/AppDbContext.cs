﻿using MicroBoost.Persistence.EfCore;
using Microsoft.EntityFrameworkCore;
using Users.DataModels.UserAggregate;

namespace Users.DataModels
{
    public sealed class AppDbContext : EfCoreContext
    {
        public DbSet<User> Users { get; set; }

        public AppDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}