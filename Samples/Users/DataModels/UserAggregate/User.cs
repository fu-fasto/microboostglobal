﻿using System;
using MicroBoost.Types;

namespace Users.DataModels.UserAggregate
{
    public class User : EntityBase<Guid>
    {
        public override Guid Id { get; set; } = Guid.NewGuid();

        public string Name { get; set; }

        public string Username { get; set; }
    }
}