using MicroBoost;
using MicroBoost.Cqrs;
using MicroBoost.Jaeger;
using MicroBoost.Metrics;
using MicroBoost.Persistence;
using MicroBoost.Swagger;
using MicroBoost.WebAPI;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Users.DataModels;

namespace Users
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services
                .AddMicroBoostBuilder()
                .AddJaeger()
                .AddWebApi()
                .AddCqrs()
                .AddSqlPersistence<AppDbContext>()
                .AddSwaggerDocs()
                .Build();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app
                .UseJaeger()
                .UseMetrics()
                .UseWebApi()
                .UseSwaggerDocs();
        }
    }
}