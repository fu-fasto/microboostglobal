﻿using System;
using System.Data;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;
using Users.DataModels.UserAggregate;

namespace Users.Commands.Handlers
{
    public class CreateUserValidator : AbstractValidator<CreateUser>
    {
        public CreateUserValidator()
        {
            RuleFor(cmd => cmd.Name).NotEmpty();
            RuleFor(cmd => cmd.Username).MinimumLength(6);
        }
    }

    public class CreateUserHandler : ICommandHandler<CreateUser>
    {
        private readonly ILogger<CreateUserHandler> _logger;
        private readonly IRepository<User, Guid> _userRepository;
        private readonly IUnitOfWork _unitOfWork;

        public CreateUserHandler(ILogger<CreateUserHandler> logger, IRepository<User, Guid> userRepository,
            IUnitOfWork unitOfWork)
        {
            _userRepository = userRepository;
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task Handle(CreateUser command, CancellationToken cancellationToken)
        {
            var user = command.MapTo<User>();

            if (await _userRepository.IsExistsAsync(x => x.Username == command.Username, cancellationToken))
            {
                throw new DataException($"User with username = {user.Username} already exists");
            }

            using var trans = await _unitOfWork.BeginTransactionAsync(cancellationToken);
            try
            {
                await _userRepository.AddAsync(user, cancellationToken);
                await trans.CommitAsync(cancellationToken);
            }
            catch (Exception)
            {
                await trans.RollbackAsync(cancellationToken);
                throw;
            }

            _logger.LogInformation("Created user with id = {Id}", command.Id);
        }
    }
}