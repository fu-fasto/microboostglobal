﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Users.Commands;
using Users.Queries;

namespace Users.Controllers
{
    [ApiController]
    [Route("users")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly ICommandBus _commandBus;
        private readonly IQueryBus _queryBus;

        public UserController(ILogger<UserController> logger, ICommandBus commandBus, IQueryBus queryBus)
        {
            _logger = logger;
            _commandBus = commandBus;
            _queryBus = queryBus;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllUsers(CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(new GetAllUsers(), cancellationToken);
            return Ok(result);
        }
        
        [HttpGet("browse")]
        public async Task<IActionResult> BrowseUsers(CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(new BrowseUsers(), cancellationToken);
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneUser([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(new GetOneUser(id), cancellationToken);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateUser([FromBody] CreateUser command, CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Ok(command.Id);
        }
    }
}