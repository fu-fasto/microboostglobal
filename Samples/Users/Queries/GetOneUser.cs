﻿using System;
using MicroBoost.Cqrs.Queries;
using Users.DataModels.UserAggregate;

namespace Users.Queries
{
    public class GetOneUser : OneQueryBase<User, Guid>
    {
        public GetOneUser(Guid id)
        {
            Id = id;
        }
    }
}