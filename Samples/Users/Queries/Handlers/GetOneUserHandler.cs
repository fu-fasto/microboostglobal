﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;
using Users.DataModels.UserAggregate;

namespace Users.Queries.Handlers
{
    public class GetOneUserHandler : IQueryHandler<GetOneUser, User>
    {
        private readonly IRepository<User, Guid> _userRepository;

        public GetOneUserHandler(IRepository<User, Guid> userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<User> Handle(GetOneUser request, CancellationToken cancellationToken)
        {
            var user = await _userRepository.FindOneAsync(request.Id, cancellationToken);
            if (user is null)
            {
                throw new NullReferenceException($"User with id={request.Id} does not exist!");
            }

            return user;
        }
    }
}