﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;
using Users.DataModels.UserAggregate;

namespace Users.Queries.Handlers
{
    public class GetAllUsersHandler : IQueryHandler<GetAllUsers, IEnumerable<User>>
    {
        private readonly IRepository<User, Guid> _userRepository;

        public GetAllUsersHandler(IRepository<User, Guid> userRepository)
        {
            _userRepository = userRepository;
        }

        public Task<IEnumerable<User>> Handle(GetAllUsers query, CancellationToken cancellationToken)
        {
            return _userRepository.FindAsync(cancellationToken: cancellationToken);
        }
    }
}