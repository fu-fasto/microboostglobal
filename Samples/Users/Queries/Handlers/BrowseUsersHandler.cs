﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;
using Users.DataModels.UserAggregate;

namespace Users.Queries.Handlers
{
    public class BrowseUsersHandler : IQueryHandler<BrowseUsers, PagedResult<User>>
    {
        private readonly IRepository<User, Guid> _userRepository;

        public BrowseUsersHandler(IRepository<User, Guid> userRepository)
        {
            _userRepository = userRepository;
        }

        public Task<PagedResult<User>> Handle(BrowseUsers request, CancellationToken cancellationToken)
        {
            var query = new FindQuery<User>();
            query.SortExpressions = new List<SortExpression<User>>
            {
                new()
                {
                    IsAscending = false,
                    Selector = x => x.Name
                }
            };
            return _userRepository.BrowseAsync(query, cancellationToken);
        }
    }
}