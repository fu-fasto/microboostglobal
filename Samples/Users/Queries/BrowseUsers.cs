﻿using MicroBoost.Cqrs.Queries;
using Users.DataModels.UserAggregate;

namespace Users.Queries
{
    public class GetAllUsers : AllQueryBase<User>
    {
    }
}