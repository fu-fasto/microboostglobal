﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Cqrs.Queries;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Tasks.Commands;
using Tasks.Queries;

namespace Tasks.Controllers
{
    [ApiController]
    [Route("tasks")]
    public class TaskController : ControllerBase
    {
        private readonly ILogger<TaskController> _logger;
        private readonly ICommandBus _commandBus;
        private readonly IQueryBus _queryBus;

        public TaskController(ILogger<TaskController> logger, ICommandBus commandBus, IQueryBus queryBus)
        {
            _logger = logger;
            _commandBus = commandBus;
            _queryBus = queryBus;
        }

        [HttpGet]
        public async Task<IActionResult> GetAllTasks(CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(new GetAllTasks(), cancellationToken);
            return Ok(result);
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetOneTask([FromRoute] Guid id, CancellationToken cancellationToken)
        {
            var result = await _queryBus.SendAsync(new GetOneTask(id), cancellationToken);
            return Ok(result);
        }

        [HttpPost]
        public async Task<IActionResult> CreateTask([FromBody] CreateTask command, CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Ok(command.Id);
        }
        
        [HttpPut]
        public async Task<IActionResult> TestTask([FromBody] TestTask command, CancellationToken cancellationToken)
        {
            await _commandBus.SendAsync(command, cancellationToken);
            return Ok(command.Id);
        }
    }
}