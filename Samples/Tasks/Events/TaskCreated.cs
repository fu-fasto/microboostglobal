﻿using System;
using MicroBoost.Events;
using MicroBoost.MessageBroker;
using Tasks.Dtos;

namespace Tasks.Events
{
    [Message(MessageScope.Internal)]
    public class TaskCreated : IEvent
    {
        [MessageKey] public Guid Id { get; set; }

        public UserDto User { get; set; }

        public string Name { get; set; }
    }
}