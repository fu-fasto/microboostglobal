﻿using System;
using MicroBoost.Types;

namespace Tasks.DataModels.TaskAggregate
{
    public class Task : EntityBase<Guid>
    {
        public override Guid Id { get; set; } = Guid.NewGuid();

        public Guid UserId { get; set; }
        
        public string Name { get; set; }
    }
}