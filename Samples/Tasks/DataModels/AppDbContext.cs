﻿using MicroBoost.Persistence.EfCore;
using Microsoft.EntityFrameworkCore;
using Tasks.DataModels.TaskAggregate;

namespace Tasks.DataModels
{
    public sealed class AppDbContext : EfCoreContext
    {
        public DbSet<Task> Tasks { get; set; }

        public AppDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }
    }
}