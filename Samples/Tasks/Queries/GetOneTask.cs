﻿using System;
using MicroBoost.Cqrs.Queries;
using Tasks.DataModels.TaskAggregate;

namespace Tasks.Queries
{
    public class GetOneTask : IQuery<Task>
    {
        public Guid Id { get; }

        public GetOneTask(Guid id)
        {
            Id = id;
        }
    }
}