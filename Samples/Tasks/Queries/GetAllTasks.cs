﻿using System.Collections.Generic;
using MicroBoost.Cqrs.Queries;
using Tasks.DataModels.TaskAggregate;

namespace Tasks.Queries
{
    public class GetAllTasks : IQuery<IEnumerable<Task>>
    {
    }
}