﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;
using Task = Tasks.DataModels.TaskAggregate.Task;

namespace Tasks.Queries.Handlers
{
    public class GetAllTasksHandler : IQueryHandler<GetAllTasks, IEnumerable<Task>>
    {
        private readonly IRepository<Task, Guid> _taskRepository;

        public GetAllTasksHandler(IRepository<Task, Guid> taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public Task<IEnumerable<Task>> Handle(GetAllTasks query, CancellationToken cancellationToken)
        {
            return _taskRepository.FindAsync(cancellationToken: cancellationToken);
        }
    }
}