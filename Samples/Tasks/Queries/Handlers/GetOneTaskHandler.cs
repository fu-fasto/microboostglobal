﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;
using Task = Tasks.DataModels.TaskAggregate.Task;

namespace Tasks.Queries.Handlers
{
    public class GetOneTaskHandler : IQueryHandler<GetOneTask, Task>
    {
        private readonly IRepository<Task, Guid> _taskRepository;

        public GetOneTaskHandler(IRepository<Task, Guid> taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public async Task<Task> Handle(GetOneTask request, CancellationToken cancellationToken)
        {
            var task = await _taskRepository.FindOneAsync(request.Id, cancellationToken);
            if (task is null)
            {
                throw new NullReferenceException($"Task with id={request.Id} does not exist!");
            }

            return task;
        }
    }
}