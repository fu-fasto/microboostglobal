﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Http;
using Tasks.Dtos;

namespace Tasks.Services
{
    public class UserServiceClient : IUserServiceClient
    {
        private readonly IHttpClient _httpClient;
        private readonly string _basePath;

        public UserServiceClient(IHttpClient httpClient, HttpClientOptions options)
        {
            _httpClient = httpClient;
            _basePath = options.Services["UserService"];
        }

        public Task<UserDto> GetAsync(Guid id, CancellationToken cancellationToken = default)
        {
            return _httpClient.GetAsync<UserDto>($"{_basePath}/users/{id}", cancellationToken);
        }
    }
}