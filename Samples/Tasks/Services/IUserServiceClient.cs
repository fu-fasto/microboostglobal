﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Tasks.Dtos;

namespace Tasks.Services
{
    public interface IUserServiceClient
    {
        Task<UserDto> GetAsync(Guid userId, CancellationToken cancellationToken = default);
    }
}