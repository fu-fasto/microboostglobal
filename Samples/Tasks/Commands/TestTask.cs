﻿using System;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace Tasks.Commands
{
    public class TestTask : CommandBase
    {
        [MessageKey]
        public Guid Id { get; set; } = Guid.NewGuid();
        public string Name { get; set; }
    }
}