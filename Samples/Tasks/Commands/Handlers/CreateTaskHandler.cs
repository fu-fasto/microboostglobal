﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.MessageBroker;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;
using Tasks.Events;
using Tasks.Services;

namespace Tasks.Commands.Handlers
{
    public class CreateTaskValidator : AbstractValidator<CreateTask>
    {
        public CreateTaskValidator()
        {
            RuleFor(cmd => cmd.UserId).NotEmpty();
            RuleFor(cmd => cmd.Name).NotEmpty();
        }
    }

    public class CreateTaskHandler : ICommandHandler<CreateTask>
    {
        private readonly ILogger<CreateTaskHandler> _logger;
        private readonly IMessagePublisher _messagePublisher;
        private readonly IRepository<DataModels.TaskAggregate.Task, Guid> _taskRepository;
        private readonly IUserServiceClient _userServiceClient;
        private readonly IUnitOfWork _unitOfWork;

        public CreateTaskHandler(ILogger<CreateTaskHandler> logger,
            IRepository<DataModels.TaskAggregate.Task, Guid> taskRepository,
            IMessagePublisher messagePublisher, IUserServiceClient userServiceClient, IUnitOfWork unitOfWork)
        {
            _taskRepository = taskRepository;
            _messagePublisher = messagePublisher;
            _userServiceClient = userServiceClient;
            _unitOfWork = unitOfWork;
            _logger = logger;
        }

        public async Task Handle(CreateTask command, CancellationToken cancellationToken)
        {
            var task = command.MapTo<DataModels.TaskAggregate.Task>();
            var user = await _userServiceClient.GetAsync(task.UserId, cancellationToken) ??
                       throw new NullReferenceException($"User with id = {task.UserId} not found!");

            task.UserId = user.Id;
            var taskCreated = new TaskCreated
            {
                Id = task.Id,
                Name = task.Name,
                User = user
            };

            using var transaction = await _unitOfWork.BeginTransactionAsync(cancellationToken);
            try
            {
                await _taskRepository.AddAsync(task, cancellationToken);
                await _messagePublisher.PublishOutBoxAsync(taskCreated, cancellationToken);
                await transaction.CommitAsync(cancellationToken);

                _logger.LogInformation("Created task with id = {Id}", task.Id);
            }
            catch (Exception)
            {
                await transaction.RollbackAsync(cancellationToken);
                throw;
            }
        }
    }
}