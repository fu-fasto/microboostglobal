﻿using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MicroBoost.Cqrs.Commands;
using Microsoft.Extensions.Logging;

namespace Tasks.Commands.Handlers
{
    public class TestTaskValidator : AbstractValidator<TestTask>
    {
        public TestTaskValidator()
        {
            RuleFor(cmd => cmd.Name).NotEmpty();
        }
    }

    public class TestTaskHandler : ICommandHandler<TestTask>
    {
        private readonly ILogger<TestTaskHandler> _logger;

        public TestTaskHandler(ILogger<TestTaskHandler> logger)
        {
            _logger = logger;
        }

        public async Task Handle(TestTask command, CancellationToken cancellationToken)
        {
            _logger.LogInformation("CC");
        }
    }
}