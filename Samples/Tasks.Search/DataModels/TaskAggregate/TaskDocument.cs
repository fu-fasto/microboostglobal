﻿using System;
using MicroBoost.Persistence.Mongo;
using MicroBoost.Types;

namespace Tasks.Search.DataModels.TaskAggregate
{
    public class TaskDocument : EntityBase<Guid>
    {
        public override Guid Id { get; set; } = Guid.NewGuid();

        [FtsIndex]
        public string Name { get; set; }

        [UniqueIndex]
        public int Number { get; set; }
    }
}