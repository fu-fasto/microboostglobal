﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Helpers;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;
using Tasks.Search.DataModels.TaskAggregate;

namespace Tasks.Search.Queries.Handlers
{
    public class GetAllTasksHandler : IQueryHandler<GetAllTasks, IEnumerable<TaskDocument>>
    {
        private readonly IRepository<TaskDocument, Guid> _taskRepository;
        private readonly ILogger<GetAllTasksHandler> _logger;

        public GetAllTasksHandler(IRepository<TaskDocument, Guid> taskRepository, ILogger<GetAllTasksHandler> logger)
        {
            _taskRepository = taskRepository;
            _logger = logger;
        }

        public Task<IEnumerable<TaskDocument>> Handle(GetAllTasks query, CancellationToken cancellationToken)
        {
            var findQuery = query.MapTo<FindQuery<TaskDocument>>();
            findQuery.Size = 20;
            findQuery.SortExpression = new() {Selector = x => x.Number};
            
            return _taskRepository.FindAsync(findQuery, cancellationToken);
        }
    }
}