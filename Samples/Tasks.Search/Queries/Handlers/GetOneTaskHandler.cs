﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Cqrs.Queries;
using MicroBoost.Persistence;
using Tasks.Search.DataModels.TaskAggregate;

namespace Tasks.Search.Queries.Handlers
{
    public class GetOneTaskHandler : IQueryHandler<GetOneTask, TaskDocument>
    {
        private readonly IRepository<TaskDocument, Guid> _taskRepository;

        public GetOneTaskHandler(IRepository<TaskDocument, Guid> taskRepository)
        {
            _taskRepository = taskRepository;
        }

        public async Task<TaskDocument> Handle(GetOneTask request, CancellationToken cancellationToken)
        {
            var task = await _taskRepository.FindOneAsync(request.Id, cancellationToken);
            if (task is null)
            {
                throw new NullReferenceException($"Task with id={request.Id} does not exist!");
            }

            return task;
        }
    }
}