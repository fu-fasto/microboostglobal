﻿using System;
using MicroBoost.Cqrs.Queries;
using Tasks.Search.DataModels.TaskAggregate;

namespace Tasks.Search.Queries
{
    public class GetOneTask : IQuery<TaskDocument>
    {
        public Guid Id { get; }

        public GetOneTask(Guid id)
        {
            Id = id;
        }
    }
}