﻿using MicroBoost.Cqrs.Queries;
using Tasks.Search.DataModels.TaskAggregate;

namespace Tasks.Search.Queries
{
    public class GetAllTasks : AllQueryBase<TaskDocument>
    {
    }
}