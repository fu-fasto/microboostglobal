﻿using System;
using MicroBoost.Cqrs.Commands;
using MicroBoost.MessageBroker;

namespace Tasks.Search.Commands
{
    public class CreateTask : CommandBase
    {
        [MessageKey] public Guid Id { get; } = Guid.NewGuid();
        public string Name { get; set; }
        public int Number { get; set; }
    }
}