﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FluentValidation;
using MicroBoost.Cqrs.Commands;
using MicroBoost.Helpers;
using MicroBoost.MessageBroker;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;
using Tasks.Search.DataModels.TaskAggregate;

namespace Tasks.Search.Commands.Handlers
{
    public class CreateTaskValidator : AbstractValidator<CreateTask>
    {
        public CreateTaskValidator()
        {
            RuleFor(cmd => cmd.Name).NotEmpty();
        }
    }

    public class CreateTaskHandler : ICommandHandler<CreateTask>
    {
        private readonly ILogger<CreateTaskHandler> _logger;
        private readonly IRepository<TaskDocument, Guid> _taskRepository;
        private readonly IMessagePublisher _messagePublisher;
        
        public CreateTaskHandler(ILogger<CreateTaskHandler> logger, IRepository<TaskDocument, Guid> taskRepository,
            IMessagePublisher messagePublisher)
        {
            _taskRepository = taskRepository;
            _messagePublisher = messagePublisher;
            _logger = logger;
        }

        public async Task Handle(CreateTask command, CancellationToken cancellationToken)
        {
            var task = command.MapTo<TaskDocument>();

            await _taskRepository.AddAsync(task, cancellationToken);
            // await _messagePublisher.PublishAsync(task.MapTo<TaskCreated>(), cancellationToken);

            _logger.LogInformation($"Created task with id = {task.Id}");
        }
    }
}