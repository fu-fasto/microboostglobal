﻿using System;
using MicroBoost.Events;
using MicroBoost.MessageBroker;
using Tasks.Search.Dtos;

namespace Tasks.Search.Events
{
    [Message(MessageScope.External)]
    public class TaskCreated : IEvent
    {
        [MessageKey] public Guid Id { get; set; }

        public UserDto User { get; set; }
        
        public string Name { get; set; }
    }
}