﻿using System;
using System.Threading;
using System.Threading.Tasks;
using MicroBoost.Events;
using MicroBoost.Helpers;
using MicroBoost.Persistence;
using Microsoft.Extensions.Logging;
using Tasks.Search.DataModels.TaskAggregate;

namespace Tasks.Search.Events.Handlers
{
    public class TaskCreatedHandler : IEventHandler<TaskCreated>
    {
        private readonly ILogger<TaskCreatedHandler> _logger;
        private readonly IRepository<TaskDocument, Guid> _taskRepository;

        public TaskCreatedHandler(IRepository<TaskDocument, Guid> taskRepository, ILogger<TaskCreatedHandler> logger)
        {
            _taskRepository = taskRepository;
            _logger = logger;
        }

        public async Task Handle(TaskCreated taskCreated, CancellationToken cancellationToken)
        {
            var task = taskCreated.MapTo<TaskDocument>();
            await _taskRepository.AddAsync(task, cancellationToken);
            _logger.LogInformation($"Processed TaskCreated with id = {taskCreated.Id}");
        }
    }
}