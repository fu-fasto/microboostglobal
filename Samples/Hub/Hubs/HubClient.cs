﻿using System;
using System.Threading.Tasks;

namespace Hub.Hubs
{
    public class HubClient : Microsoft.AspNetCore.SignalR.Hub
    {
        public override async Task OnConnectedAsync()
        {
            Console.WriteLine(Context);
        }

        public override async Task OnDisconnectedAsync(Exception? exception)
        {
            
        }
    }
}