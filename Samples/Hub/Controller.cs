﻿using Hub.Hubs;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace Hub
{
    [ApiController]
    [Route("")]
    public class Controller : ControllerBase
    {
        IHubContext<HubClient> _hub;

        public Controller(IHubContext<HubClient> hub)
        {
            _hub = hub;
        }


        [HttpGet]
        public IActionResult GetAllUsers()
        {
            _hub.Clients.All.SendAsync("Han", "Satthan", "fd");
            return Ok("DONE");
        }
    }
}